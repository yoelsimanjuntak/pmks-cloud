# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.36-MariaDB)
# Database: hh_pmks
# Generation Time: 2020-07-31 20:39:12 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _roles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_roles`;

CREATE TABLE `_roles` (
  `RoleID` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_roles` WRITE;
/*!40000 ALTER TABLE `_roles` DISABLE KEYS */;

INSERT INTO `_roles` (`RoleID`, `RoleName`)
VALUES
	(1,'Administrator'),
	(2,'Operator');

/*!40000 ALTER TABLE `_roles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_settings`;

CREATE TABLE `_settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_settings` WRITE;
/*!40000 ALTER TABLE `_settings` DISABLE KEYS */;

INSERT INTO `_settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','PMKS Cloud'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Pusat Data PMKS Kab. Humbang Hasundutan'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','Dinas Sosial Pemerintah Kabupaten Humbang Hasundutan'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','-'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT',''),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG',''),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','-'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','-'),
	(11,'SETTING_WEB_API_FOOTERLINK','SETTING_WEB_API_FOOTERLINK','-'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','logo.png'),
	(13,'SETTING_WEB_SKIN_CLASS','SETTING_WEB_SKIN_CLASS','skin-green-light'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','loader-128x/Preloader_2.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0');

/*!40000 ALTER TABLE `_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_users`;

CREATE TABLE `_users` (
  `UserID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Password` varchar(200) NOT NULL DEFAULT '',
  `RoleID` bigint(10) NOT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) DEFAULT NULL,
  `Email` varchar(200) NOT NULL DEFAULT '',
  `Nama` varchar(200) NOT NULL DEFAULT '',
  `NIK` varchar(200) DEFAULT NULL,
  `NoTelp` varchar(200) DEFAULT NULL,
  `Alamat` text,
  `TanggalLahir` date DEFAULT NULL,
  `Foto` text,
  `FotoKTP` text,
  `FotoKTP2` text,
  `TanggalRegistrasi` date DEFAULT NULL,
  `IsEmailVerified` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `_users` WRITE;
/*!40000 ALTER TABLE `_users` DISABLE KEYS */;

INSERT INTO `_users` (`UserID`, `Password`, `RoleID`, `LastLogin`, `IsSuspend`, `Email`, `Nama`, `NIK`, `NoTelp`, `Alamat`, `TanggalLahir`, `Foto`, `FotoKTP`, `FotoKTP2`, `TanggalRegistrasi`, `IsEmailVerified`)
VALUES
	(2,'e10adc3949ba59abbe56e057f20f883e',1,NULL,NULL,'admin','Administrator',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mdesa
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mdesa`;

CREATE TABLE `mdesa` (
  `KdDesa` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `KdKecamatan` bigint(10) unsigned DEFAULT NULL,
  `NmDesa` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`KdDesa`),
  KEY `FK_Desa_Kecamatan` (`KdKecamatan`),
  CONSTRAINT `FK_Desa_Kecamatan` FOREIGN KEY (`KdKecamatan`) REFERENCES `mkecamatan` (`KdKecamatan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mdesa` WRITE;
/*!40000 ALTER TABLE `mdesa` DISABLE KEYS */;

INSERT INTO `mdesa` (`KdDesa`, `KdKecamatan`, `NmDesa`)
VALUES
	(5,1,'DESA 1'),
	(6,1,'DESA 2'),
	(7,1,'DESA 3'),
	(8,2,'DESA 1');

/*!40000 ALTER TABLE `mdesa` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mkategori
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mkategori`;

CREATE TABLE `mkategori` (
  `KdKategori` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmKategori` varchar(200) DEFAULT NULL,
  `Seq` int(10) DEFAULT NULL,
  PRIMARY KEY (`KdKategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mkategori` WRITE;
/*!40000 ALTER TABLE `mkategori` DISABLE KEYS */;

INSERT INTO `mkategori` (`KdKategori`, `NmKategori`, `Seq`)
VALUES
	(1,'Anak Balita Terlantar (<5 Tahun)',2),
	(2,'Anak Terlantar (6 sampai 18 Tahun)',3),
	(3,'Anak yang Berhadapan dengan Hukum (12 sampai 18 Tahun)',4),
	(4,'Anak Jalanan',5),
	(5,'Anak dengan Kedisabilitasan (<18)',6),
	(6,'Anak yang Menjadi Korban Tindak Kekerasan atau Diperlakukan salah\n(<18 Tahun)',7),
	(7,'Anak yang Memerlukan Perlindungan Khusus (6 sampai 18 Tahun)',8),
	(9,'Penyandang Disabilitas',9),
	(10,'Tuna Susila',10),
	(11,'Gelandangan',11),
	(12,'Pengemis',12),
	(13,'Pemulung',13),
	(14,'Bekas Warga Binan Lembaga Pemasyarakatan',14),
	(15,'Orang dengan HIV/AIDS',15),
	(16,'Korban Penyalahgunaan Napza',16),
	(17,'Korban Trafficking',17),
	(18,'Korban Tindak Kekerasan',18),
	(19,'Pekerjaan Migran Bermasalah Sosial',19),
	(20,'Korban Bencana Alam',20),
	(21,'Korban Bencana Sosial',21),
	(22,'Perempuan Rawan Sosial Ekonomi (18 ? 59 Tahun)',22),
	(23,'Fakir Miskin',23),
	(24,'Keluarga Bermasalah Sosial Psikologis',24),
	(26,'Lanjut Usia Terlantar (>60 Tahun)',1);

/*!40000 ALTER TABLE `mkategori` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mkecamatan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mkecamatan`;

CREATE TABLE `mkecamatan` (
  `KdKecamatan` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `NmKecamatan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`KdKecamatan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mkecamatan` WRITE;
/*!40000 ALTER TABLE `mkecamatan` DISABLE KEYS */;

INSERT INTO `mkecamatan` (`KdKecamatan`, `NmKecamatan`)
VALUES
	(1,'DOLOKSANGGUL'),
	(2,'LINTONG NI HUTA'),
	(3,'BAKTIRAJA'),
	(4,'POLLUNG'),
	(5,'PARLILITAN'),
	(6,'PARANGINAN'),
	(7,'PAKKAT'),
	(8,'ONAN GANJANG'),
	(9,'TARABINTANG'),
	(10,'SIJAMAPOLANG');

/*!40000 ALTER TABLE `mkecamatan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mkolom
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mkolom`;

CREATE TABLE `mkolom` (
  `KdKolom` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `KdParent` bigint(10) unsigned DEFAULT NULL,
  `KdCondition` text,
  `KdKategori` bigint(10) unsigned NOT NULL,
  `Seq` int(10) DEFAULT NULL,
  `NmKolom` varchar(50) NOT NULL DEFAULT '',
  `IsAllowEmpty` tinyint(1) NOT NULL DEFAULT '1',
  `NmTipe` varchar(50) NOT NULL DEFAULT '',
  `NmOption` text,
  PRIMARY KEY (`KdKolom`),
  KEY `FK_Kolom_Kategori` (`KdKategori`),
  KEY `FK_Kolom_Parent` (`KdParent`),
  CONSTRAINT `FK_Kolom_Kategori` FOREIGN KEY (`KdKategori`) REFERENCES `mkategori` (`KdKategori`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Kolom_Parent` FOREIGN KEY (`KdParent`) REFERENCES `mkolom` (`KdKolom`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mkolom` WRITE;
/*!40000 ALTER TABLE `mkolom` DISABLE KEYS */;

INSERT INTO `mkolom` (`KdKolom`, `KdParent`, `KdCondition`, `KdKategori`, `Seq`, `NmKolom`, `IsAllowEmpty`, `NmTipe`, `NmOption`)
VALUES
	(28,NULL,NULL,1,1,'Keadaan Ekonomi Pengasuh',0,'SELECT','[\"Miskin\",\"Sangat Miskin\",\"Mampu\"]'),
	(29,NULL,NULL,1,2,'Pekerjaan Pengasuh',0,'SELECT','[\"Wiraswasta\",\"Petani / Pekebun\",\"Buruh Kasar\"]'),
	(30,NULL,NULL,1,3,'Riwayat Kesehatan',0,'SELECT','[\"Mengalami Gizi Buruk\",\"Mengalami Gizi Kurang\"]'),
	(31,NULL,NULL,1,4,'Makan Dalam Sehari',0,'SELECT','[\"1 kali Sehari\",\"2 kali Sehari\",\"3 kali Sehari\"]'),
	(32,NULL,NULL,1,5,'Bantuan Diterima',0,'SELECT','[\"PKH (Program Keluarga Harapan)\",\"Program Indonesia Sehat\",\"Tidak Ada\"]'),
	(33,NULL,NULL,2,1,'Keadaan Ekonomi Pengasuh',0,'SELECT','[\"Miskin\",\"Sangat Miskin\",\"Mampu\"]'),
	(34,NULL,NULL,2,2,'Pekerjaan Pengasuh',0,'SELECT','[\"Wiraswasta\",\"Petani / Pekebun\",\"Buruh Kasar\"]'),
	(36,NULL,NULL,2,3,'Makan Dalam Sehari',0,'SELECT','[\"1 kali Sehari\",\"2 kali Sehari\",\"3 kali Sehari\"]'),
	(37,NULL,NULL,2,4,'Bantuan Diterima',0,'SELECT','[\"PKH (Program Keluarga Harapan)\",\"Program Indonesia Sehat\",\"Tidak Ada\"]'),
	(39,NULL,NULL,5,1,'Jumlah Anggota Keluarga',0,'TEXT','[]'),
	(40,NULL,NULL,5,2,'Disabilitas yang Disandang',0,'SELECT','[\" Disabilitas Fisik\",\"Disabilitas Intelektual\",\"Disabilitas Mental\",\"Disabilitas Sensorik\"]'),
	(41,40,' Disabilitas Fisik',5,1,'Jenis Disabilitas Fisik',1,'SELECT','[\"Amputasi\",\"Lumpuh Layu\\/Kaku\",\"Paraplegi\",\"Cerebral Palsy (CP)\",\"Pasca Lepra\\/Kusta\",\"Orang Kecil \\/ Kerdil\"]'),
	(42,40,'Disabilitas Intelektual',5,1,'Jenis Disabilitas Intelektual',1,'SELECT','[\"Lambat Belajar\",\"Tuna Grahita\",\"Down Syndrom\"]'),
	(43,40,'Disabilitas Mental',5,1,'Jenis Disabilitas Mental',0,'SELECT','[\"Psikososial\",\"Disabilitas Perkembangan\"]'),
	(44,40,'Disabilitas Sensorik',5,1,'Jenis Disabilitas Sensorik',1,'SELECT','[\"Disabilitas Netra\",\"Disabilitas Rungu\",\"Disabilitas Wicara\"]'),
	(45,43,'Psikososial',5,1,'Jenis Psikososial',1,'SELECT','[\"Skizofrenia\",\"Bipolar\",\"Depresi\",\"Anxietas\",\"Gangguan Kepribadian\"]'),
	(46,43,'Disabilitas Perkembangan',5,1,'Jenis Disabilitas Perkembangan',1,'SELECT','[\"Autis\",\"Hiperaktif\"]'),
	(47,NULL,NULL,5,3,'Keadaan Ekonomi',0,'SELECT','[\"Mampu\",\"Miskin\",\"Sangat Miskin\"]'),
	(48,NULL,NULL,5,4,'Jenis Pekerjaan',0,'SELECT','[\"Wiraswasta\",\"Petani/Pekebun\",\"Buruh Kasar\"]'),
	(49,NULL,NULL,5,5,'Penyebab Kedisabilitasan',0,'SELECT','[\"Bawaan Lahir\",\"Sakit\",\"Kecelakaan\"]'),
	(50,NULL,NULL,5,6,'Pemenuhan Kebutuhan Dasar',0,'SELECT','[\"Sangat Tergantung Dengan Bantuan Orang Lain\",\"Tergantung Dengan Bantuan Orang Lain\",\"Tidak Tergantung Dengan Bantuan Orang Lain\"]'),
	(51,NULL,NULL,5,7,'Tingkat Kedisabilitasan',0,'SELECT','[\"Berat\",\"Sedang\",\"Rendah\"]'),
	(52,NULL,NULL,5,8,'Bantuan Diterima',0,'SELECT','[\"PKH (Program Keluarga Harapan)\",\"Program Indonesia Sehat\",\"Tidak Ada\"]'),
	(53,NULL,NULL,5,9,'Alat Bantu yang Dibutuhkan',0,'SELECT','[\"Kursi Roda\",\"Kruk\",\"Tongkat\",\"Alat Bantu Dengar\",\"Tongkat Lipat\"]');

/*!40000 ALTER TABLE `mkolom` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tdata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tdata`;

CREATE TABLE `tdata` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `KdKategori` bigint(10) unsigned NOT NULL,
  `NoNIK` varchar(50) NOT NULL DEFAULT '',
  `NoKK` varchar(50) NOT NULL DEFAULT '',
  `NmNama` varchar(200) NOT NULL DEFAULT '',
  `NmAlamat` varchar(200) DEFAULT NULL,
  `NmDusun` varchar(200) DEFAULT NULL,
  `KdKecamatan` bigint(10) unsigned NOT NULL,
  `KdDesa` bigint(10) unsigned NOT NULL,
  `NmTempatLahir` varchar(200) DEFAULT NULL,
  `TglLahir` date NOT NULL,
  `NmJenisKelamin` varchar(50) DEFAULT NULL,
  `NmAyah` varchar(50) DEFAULT NULL,
  `NmIbu` varchar(50) DEFAULT NULL,
  `NmPendidikan` varchar(200) DEFAULT NULL,
  `NmRekanTinggal` varchar(200) DEFAULT NULL,
  `NmFoto` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_Data_Kecamatan` (`KdKecamatan`),
  KEY `FK_Data_Desa` (`KdDesa`),
  KEY `FK_Kategori` (`KdKategori`),
  CONSTRAINT `FK_Data_Desa` FOREIGN KEY (`KdDesa`) REFERENCES `mdesa` (`KdDesa`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Data_Kecamatan` FOREIGN KEY (`KdKecamatan`) REFERENCES `mkecamatan` (`KdKecamatan`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Kategori` FOREIGN KEY (`KdKategori`) REFERENCES `mkategori` (`KdKategori`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tdata_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tdata_detail`;

CREATE TABLE `tdata_detail` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `KdData` bigint(10) unsigned NOT NULL,
  `KdKolom` bigint(10) unsigned NOT NULL,
  `NmValue` text NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_Detail_Data` (`KdData`),
  KEY `FK_Detail_Kolom` (`KdKolom`),
  CONSTRAINT `FK_Detail_Data` FOREIGN KEY (`KdData`) REFERENCES `tdata` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Detail_Kolom` FOREIGN KEY (`KdKolom`) REFERENCES `mkolom` (`KdKolom`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
