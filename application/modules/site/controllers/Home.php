<?php
class Home extends MY_Controller {
  function __construct() {
    parent::__construct();
  }

  function index(){
    if(!IsLogin()) {
      redirect('admin/dashboard/login');
    } else {
      redirect('admin/dashboard');
    }
  }

  function _404(){
    echo '404';
  }
}
?>
