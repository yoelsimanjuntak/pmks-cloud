<table id="tbl-menu" class="table table-striped">
  <thead>
    <tr>
      <th>
        <a href="<?=site_url('admin/business/add-menu/'.$data[COL_IDRESTO])?>" class="btn btn-xs btn-outline-success btn-popup-menu"><i class="fas fa-plus"></i></a>
      </th>
      <th>Menu</th>
      <th>Keterangan</th>
      <th>Harga</th>
      <th class="text-center">Tersedia</th>
      <th class="text-center">Aktif</th>
    </tr>
  </thead>
  <tbody>
    <?php
    if(!empty($menu)) {
      foreach($menu as $m) {
        ?>
        <tr>
          <td style="width: 10px; white-space: nowrap">
            <a href="<?=site_url('admin/business/edit-menu/'.$m[COL_IDMENU])?>" class="btn btn-xs btn-outline-success btn-popup-menu"><i class="fas fa-pencil"></i></a>&nbsp;
            <a href="<?=site_url('admin/business/delete-menu/'.$m[COL_IDMENU])?>" class="btn btn-xs btn-outline-danger btn-action-menu"><i class="fas fa-trash"></i></a>
          </td>
          <td><a href="<?=MY_UPLOADURL.$m[COL_FILEGAMBAR]?>" data-menu="<?=$m[COL_MENU]?>" class="btn-popup-image"><?=$m[COL_MENU]?></a></td>
          <td><?=$m[COL_DESKRIPSI]?></td>
          <td class="text-right"><?=number_format($m[COL_HARGA])?></td>
          <td class="text-center"><?=!empty($m[COL_ISTERSEDIA])?'<i class="far fa-check"></i>':'<i class="far fa-times"></i>'?></td>
          <td class="text-center"><?=!empty($m[COL_ISAKTIF])?'<i class="far fa-check"></i>':'<i class="far fa-times"></i>'?></td>
        </tr>
        <?php
      }
    } else {
      echo '<tr><td colspan="7" class="text-center font-italic">Belum ada data.</td></tr>';
    }
    ?>
  </tbody>
</table>
<div class="modal fade" id="modal-menugambar" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Menu</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  var tblmenu = $('#tbl-menu');
  var modalmenu = $('#modal-menu');
  var modalgambar = $('#modal-menugambar');

  $('.modal').on('hidden.bs.modal', function (e) {
    $('.modal-body', $(this)).empty();
  });

  $('.btn-popup-image', tblmenu).click(function() {
    var href = $(this).attr('href');
    var menu = $(this).data('menu');
    $('.modal-body', modalgambar).html('<img src="'+href+'" style="max-width: 100%" />');
    $('.modal-title', modalgambar).html(menu);
    modalgambar.modal('show');

    return false;
  });

  $('.btn-popup-menu', tblmenu).click(function() {
    var url = $(this).attr('href');
    $('.modal-body', modalmenu).load(url, function() {
      $('.uang', modalmenu).number(true, 0, '.', ',');
      $('form', modalmenu).validate({
        ignore: "[type=file]",
        submitHandler: function(form) {
          var btnSubmit = $('button[type=submit]', $(form));
          btnSubmit.attr('disabled', true);
          $(form).ajaxSubmit({
            dataType: 'json',
            type : 'post',
            success: function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success('Berhasil');
                modalmenu.modal('hide');
                $('[data-card-widget="card-refresh"]').click();
              }
            },
            error: function() {
              toastr.error('SERVER ERROR');
            },
            complete: function() {
              btnSubmit.attr('disabled', false);
            }
          });
          return false;
        }
      });
      modalmenu.modal('show');
    });
    return false;
  });

  $('.btn-action-menu', tblmenu).click(function() {
    var url = $(this).attr('href');
    if(confirm('Apakah anda yakin?')) {
      $.get(url, function(res) {
        if(res.error != 0) {
          toastr.error(res.error);
        } else {
          toastr.success('Berhasil');
        }
      }, "json").done(function() {
        $('[data-card-widget="card-refresh"]').click();
      }).fail(function() {
        toastr.error('SERVER ERROR');
      });
    }
    return false;
  });
});
</script>
