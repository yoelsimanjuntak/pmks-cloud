<?php
$arrschedule = array();
$rschedule = $this->db
->join(TBL_M_HARI,TBL_M_HARI.'.'.COL_IDHARI." = ".TBL_M_RESTOJADWAL.".".COL_IDHARI,"left")
->where(COL_IDRESTO, $data[COL_IDRESTO])
->order_by(TBL_M_RESTOJADWAL.'.'.COL_IDHARI)
->get(TBL_M_RESTOJADWAL)
->result_array();
foreach ($rschedule as $s) {
  $arrschedule[] = array(
    'Day' => $s[COL_IDHARI],
    'DayText' => $s[COL_HARI],
    'TimeFrom' => $s[COL_JAMBUKA],
    'TimeTo' => $s[COL_JAMTUTUP]
  );
}
?>
<style>
#map {
    height: 400px;  /* The height is 400 pixels */
    width: 100%;  /* The width is the width of the web page */
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?= $title ?></h3>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url('admin/dashboard')?>"><i class="fa fa-home"></i> Home</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('admin/business/index')?>">Merchant</a></li>
          <li class="breadcrumb-item active">Form</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <a href="<?=site_url('admin/business/index')?>" class="btn btn-sm btn-secondary"><i class="far fa-arrow-left"></i>&nbsp;KEMBALI</a>
          <button type="submit" class="btn btn-sm btn-success"><i class="far fa-check"></i>&nbsp;SIMPAN</button>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-olive">
          <div class="card-header">
            <h5 class="card-title font-weight-light">Informasi Bisnis</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Nama Usaha</label>
                  <input type="hidden" name="<?=COL_IDRESTO?>" value="<?=!empty($data)?$data[COL_IDRESTO]:''?>" />
                  <input type="text" class="form-control" name="BusinessName" placeholder="Nama Usaha" value="<?=!empty($data)?$data[COL_NAMA]:''?>" required />
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <textarea class="form-control" name="BusinessAddress" placeholder="Alamat" required><?=!empty($data)?$data[COL_ALAMAT]:''?></textarea>
                </div>
                <div class="form-group">
                  <label>Latitude / Longitude <small><a href="#" data-toggle="modal" data-target="#modal-koordinat">Tentukan koordinat</a></small></label>
                  <div class="row">
                    <div class="col-sm-6">
                      <input type="text" class="form-control" id="lat" name="<?=COL_LAT?>" value="<?=!empty($data)?$data[COL_LAT]:''?>" placeholder="Lat" required readonly />
                    </div>
                    <div class="col-sm-6">
                      <input type="text" class="form-control" id="lng" name="<?=COL_LONG?>" value="<?=!empty($data)?$data[COL_LONG]:''?>" placeholder="Long" required readonly />
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="icheck-olive d-block mr-2">
                    <input type="checkbox" id="isHalal" name="<?=COL_ISHALAL?>" <?=!empty($data)&&$data[COL_ISHALAL]?'checked':''?>>
                    <label for="isHalal">HALAL</label>
                  </div>
                  <div class="icheck-olive d-block mr-2">
                    <input type="checkbox" id="isBuka" name="<?=COL_ISBUKA?>" <?=!empty($data)&&$data[COL_ISBUKA]?'checked':''?>>
                    <label for="isBuka">BUKA</label>
                  </div>
                  <div class="icheck-olive d-block mr-2">
                    <input type="checkbox" id="isAktif" name="<?=COL_ISAKTIF?>" <?=!empty($data)&&$data[COL_ISAKTIF]?'checked':''?>>
                    <label for="isAktif">AKTIF</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-olive">
          <div class="card-header">
            <h5 class="card-title font-weight-light">Jadwal</h5>
          </div>
          <div class="card-body p-0">
            <input type="hidden" name="JadwalBuka" />
            <table id="tbl-jadwal" class="table table-striped">
              <thead>
                <tr>
                  <th>Hari</th>
                  <th class="text-center">Jam Buka</th>
                  <th class="text-center">Jam Tutup</th>
                  <th class="text-center">#</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
              <tfoot>
                <td>
                  <select name="Day" class="form-control form-control-sm no-select2" style="min-width: 10vw">
                    <?=GetCombobox("SELECT * from m_hari order by IdHari", COL_IDHARI, COL_HARI, null, true, false, '-- Hari --')?>
                  </select>
                </td>
                <td class="text-center" style="white-space: nowrap">
                  <input type="text" class="form-control form-control-sm d-inline-block timepicker text-center" id="timepickerFrom" name="TimeFrom" placeholder="HH:MM" style="width: 6vw" readonly />
                </td>
                <td class="text-center" style="white-space: nowrap">
                  <input type="text" class="form-control form-control-sm d-inline-block timepicker text-center" id="timepickerTo" name="TimeTo" placeholder="HH:MM" style="width: 6vw" readonly />
                </td>
                <td class="text-center" style="padding-right: 1.5rem!important">
                  <button type="button" id="btn-add-schedule" class="btn btn-xs btn-outline-secondary"><i class="fas fa-plus"></i></button>
                </td>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
      <div class="col-sm-12">
        <div class="card card-outline card-olive">
          <div class="card-header">
            <h5 class="card-title font-weight-light">Menu</h5>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="card-refresh" data-source="<?=site_url('admin/business/menu/'.$data[COL_IDRESTO])?>"><i class="fas fa-sync-alt"></i></button>
            </div>
          </div>
          <div class="card-body p-0">
          </div>
        </div>
      </div>
    </div>
    <?=form_close()?>
  </div>
</section>

<div class="modal fade" id="modal-koordinat" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Koordinat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <p class="font-italic"><span class="text-red">*)</span> Klik pada map untuk menandai lokasi</p>
        <div id="map"></div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-menu" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Menu</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<script>
var map; //Will contain map object.
var marker = false; ////Has the user plotted their location marker?

//Function called to initialize / create the map.
//This is called when the page has loaded.
function initMap() {
    var currLat = $("#lat").val();
    var currLng = $("#lng").val();

    //The center location of our map.
    var centerOfMap = new google.maps.LatLng(2.2587974,98.7436999);
    if(currLat && currLng) {
        centerOfMap = new google.maps.LatLng(parseFloat(currLat), parseFloat(currLng));
    }

    //Map options.
    var options = {
        center: centerOfMap, //Set center.
        zoom: 14 //The zoom value.
    };

    //Create the map object.
    map = new google.maps.Map(document.getElementById('map'), options);

    if(currLat && currLng) {
        var currLatLng = {lat: parseFloat(currLat),lng: parseFloat(currLng)};
        var currLocation = new google.maps.Marker({
            position: currLatLng,
            map: map,
            title: 'Lokasi sekarang',
            icon: {
                url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png"
            }
        });
    }


    //Listen for any clicks on the map.
    google.maps.event.addListener(map, 'click', function(event) {
        //Get the location that the user clicked.
        var clickedLocation = event.latLng;
        //If the marker hasn't been added.
        if(marker === false){
            //Create the marker.
            marker = new google.maps.Marker({
                position: clickedLocation,
                map: map,
                draggable: true //make it draggable
            });
            //Listen for drag events!
            google.maps.event.addListener(marker, 'dragend', function(event){
                markerLocation();
            });
        } else{
            //Marker has already been added, so just change its location.
            marker.setPosition(clickedLocation);
        }
        //Get the marker's location.
        markerLocation();
    });
}

//This function will get the marker's current location and then add the lat/long
//values to our textfields so that we can save the location.
function markerLocation(){
    //Get location.
    var currentLocation = marker.getPosition();
    //Add lat and lng values to a field that we can save.
    document.getElementById('lat').value = currentLocation.lat(); //latitude
    document.getElementById('lng').value = currentLocation.lng(); //longitude
    $('#map').closest('.modal').modal('hide');
}

$('[name=<?=COL_LAT?>], [name=<?=COL_LONG?>]').change(function() {
    var lat = $('[name=<?=COL_LAT?>]').val();
    var long = $('[name=<?=COL_LONG?>]').val();

    if(lat && long) {
        var clickedLocation = {lat: parseFloat(lat),lng: parseFloat(long)};
        if(marker === false){
            marker = new google.maps.Marker({
                position: clickedLocation,
                map: map,
                draggable: true
            });
            google.maps.event.addListener(marker, 'dragend', function(event){
                markerLocation();
            });
        } else{
            marker.setPosition(clickedLocation);
        }
        map.setCenter(clickedLocation);
    }
});

function writeSchedule(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      arr = arr.sort(function (a, b) {
        return (a['Day']).localeCompare(b['Day']);
      });
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].DayText+'</td>';
        html += '<td class="text-center">'+arr[i].TimeFrom+'</td>';
        html += '<td class="text-center">'+arr[i].TimeTo+'</td>';
        html += '<td class="text-center"><button type="button" class="btn btn-xs btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].Day+'" /></td>';
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return (e.Day != idx); });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="4"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="4"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}

$(document).ready(function() {
  $('[data-card-widget="card-refresh"]').click();
  $('[name=JadwalBuka]').change(function() {
    writeSchedule('tbl-jadwal', 'JadwalBuka');
  }).val(encodeURIComponent('<?=!empty($arrschedule)?json_encode($arrschedule):''?>')).trigger('change');

  $('#btn-add-schedule').click(function() {
    var dis = $(this);
    var arr = $('[name=JadwalBuka]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var day = $('[name=Day]', row).val();
    var timeFr = $('[name=TimeFrom]', row).val();
    var timeTo = $('[name=TimeTo]', row).val();
    if(day && timeFr && timeTo) {
      var exist = jQuery.grep(arr, function(a) {
        return a.Day == day;
      });
      if(exist.length == 0) {
        arr.push({'Day': day, 'DayText': $('[name=Day] option:selected', row).html(), 'TimeFrom':timeFr, 'TimeTo':timeTo});
        $('[name=JadwalBuka]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('[name=TimeFrom]', row).val('07:00').trigger('change');
        $('[name=TimeTo]', row).val('07:00').trigger('change');
        $('select', row).val('').trigger('change');
      }
    } else {
      alert('Harap isi jadwal dengan benar.');
    }
  });

  $('#form-main').validate({
    ignore: "[type=file]",
    submitHandler: function(form) {
      if(!confirm('Apakah anda yakin?')) {
        return false;
      }

      var btnSubmit = $('button[type=submit], button.btn-verify', $(form));
      btnSubmit.attr('disabled', true);
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success('Berhasil');
            if(res.data && res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDzvhEg4efwXcYvdMvRLDGUEau4vjh8klg&callback=initMap">
</script>
