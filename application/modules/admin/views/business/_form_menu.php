<?=form_open_multipart(current_url(),array('role'=>'form','id'=>'form-menu','class'=>'form-horizontal'))?>
<div class="row" style="margin-right: -7.5px">
  <div class="col-sm-12">
    <div class="form-group">
      <label>Nama Menu</label>
      <input type="text" class="form-control" name="<?=COL_MENU?>" value="<?=!empty($menu)?$menu[COL_MENU]:''?>" />
    </div>
    <div class="form-group">
      <label>Keterangan</label>
      <textarea class="form-control" name="<?=COL_DESKRIPSI?>"><?=!empty($menu)?$menu[COL_DESKRIPSI]:''?></textarea>
    </div>
    <div class="form-group">
      <label>Gambar</label>
      <input type="file" class="d-block" name="<?=COL_FILEGAMBAR?>" accept=".jpg,.png,.jpeg" />
    </div>
    <div class="form-group row">
      <div class="col-sm-6">
        <label>Harga</label>
        <input type="text" class="form-control text-right uang" name="<?=COL_HARGA?>" value="<?=!empty($menu)?$menu[COL_HARGA]:''?>" />
      </div>
      <div class="col-sm-6">
        <label>Status</label>
        <div class="d-block">
          <div class="icheck-olive d-inline-block mr-2">
            <input type="checkbox" id="isMenuTersedia" name="<?=COL_ISTERSEDIA?>" <?=!empty($menu)&&$menu[COL_ISTERSEDIA]?'checked':''?>>
            <label for="isMenuTersedia" class="font-weight-normal">TERSEDIA</label>
          </div>
          <div class="icheck-olive d-inline-block mr-2">
            <input type="checkbox" id="isMenuAktif" name="<?=COL_ISAKTIF?>" <?=!empty($menu)&&$menu[COL_ISAKTIF]?'checked':''?>>
            <label for="isMenuAktif" class="font-weight-normal">AKTIF</label>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row" style="border-top: 1px solid #f4f4f4; margin-left: -15px; margin-right: -15px">
  <div class="col-sm-12 pt-3">
    <div class="form-group text-center mb-0">
      <button type="submit" class="btn btn-sm btn-success"><i class="far fa-save"></i>&nbsp;SIMPAN</button>
    </div>
  </div>
</div>
<?=form_close()?>
