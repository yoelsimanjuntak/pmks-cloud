<?php
if(!empty($cetak)) {
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=PMKS - Rekapitulasi Kabupaten ".date('YmdHi').".xls");
  ?>
  <style>
  td, th {
    background: transparent;
    border: 0.5px solid #000;
  }
  </style>
  <?php
}
?>
<table class="table table-bordered table-responsive" style="font-size: 10pt">
  <thead>
    <tr>
      <th rowspan="2" style="vertical-align: middle; text-align: center">No.</th>
      <th rowspan="2" style="vertical-align: middle; text-align: center">Kecamatan</th>
      <th colspan="<?=count($rkategori)?>" style="text-align: center">JENIS PMKS</th>
      <th rowspan="2" style="vertical-align: middle">JUMLAH</th>
    </tr>
    <tr>
      <?php
      foreach($rkategori as $k) {
        ?>
        <th style="text-align: center; vertical-align: middle"><?=$k[COL_NMKATEGORI]?></th>
        <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
    $no = 1;
    $total = array();
    foreach ($res as $r) {
      ?>
      <tr>
        <td class="text-center"><?=$no?></td>
        <td><?=$r[COL_NMKECAMATAN]?></td>
        <?php
        foreach($rkategori as $k) {
          ?>
          <td class="text-right" style="vertical-align: middle"><?=number_format($r["Total_".$k[COL_KDKATEGORI]])?></td>
          <?php
          if(!empty($total[$k[COL_KDKATEGORI]])) $total[$k[COL_KDKATEGORI]] += $r["Total_".$k[COL_KDKATEGORI]];
          else $total[$k[COL_KDKATEGORI]] = $r["Total_".$k[COL_KDKATEGORI]];
        }
        ?>
        <td class="text-right" style="vertical-align: middle"><?=number_format($r["Total"])?></td>
      </tr>
      <?php
      $no++;
      if(!empty($total["Total"])) $total["Total"] += $r["Total"];
      else $total["Total"] = $r["Total"];
    }
    ?>
  </tbody>
  <tfoot>
    <tr>
      <th class="text-right" colspan="2">JUMLAH</th>
      <?php
      foreach($rkategori as $k) {
        ?>
        <th class="text-right" style="vertical-align: middle"><?=number_format($total[$k[COL_KDKATEGORI]])?></th>
        <?php
      }
      ?>
      <th class="text-right" style="vertical-align: middle"><?=number_format($total["Total"])?></th>
    </tr>
  </tfoot>
</table>
