<?php
$arrkol = array(
  'KK'=>'No. KK',
  'TTL'=>'TEMPAT & TANGGAL LAHIR',
  'ALM'=>'ALAMAT',
  'JK'=>'JENIS KELAMIN',
  'US'=>'USIA',
  'PEN'=>'PENDIDIKAN'
);
$arrnmkol = array(
  'KK'=>COL_NOKK,
  'TTL'=>COL_NMNAMA,
  'ALM'=>COL_NMALAMAT,
  'JK'=>COL_NMJENISKELAMIN,
  'US'=>COL_NMNAMA,
  'PEN'=>COL_NMPENDIDIKAN
);
if(!empty($cetak)) {
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=PMKS - Rekapitulasi ".$rkategori[COL_NMKATEGORI]." ".date('YmdHi').".xls");
  ?>
  <style>
  td, th {
    background: transparent;
    border: 0.5px solid #000;
  }
  </style>
  <?php
}
?>
<style>
    td.nik {
      mso-number-format:General;
      mso-number-format:"\@";/*force text*/
    }
</style>
<table class="table table-bordered table-responsive" style="font-size: 10pt">
  <thead>
    <tr>
      <th style="text-align: center">No.</th>
      <th style="text-align: center">NAMA</th>
      <th style="text-align: center">NIK</th>
      <th style="text-align: center">No. KK</th>
      <th style="text-align: center">Tempat & Tgl. Lahir</th>
      <th style="text-align: center">Jenis Kelamin</th>
      <th style="text-align: center">Usia</th>
      <th style="text-align: center">Pendidikan</th>
      <th style="text-align: center">Desa</th>
      <th style="text-align: center">Alamat</th>
      <?php
      /*foreach($kol as $k) {
        ?>
        <th style="text-align: center"><?=$arrkol[$k]?></th>
        <?php
      }*/
      foreach($kdKolom as $k) {
        $rkolom = $this->db
        ->where(COL_KDKOLOM, $k)
        ->get(TBL_MKOLOM)
        ->row_array();
        ?>
        <th style="text-align: center"><?=!empty($rkolom)?strtoupper($rkolom[COL_NMKOLOM]):'-'?></th>
        <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
    $no = 1;
    foreach ($res as $r) {
      $diff = abs(strtotime(date('Y-m-d')) - strtotime($r[COL_TGLLAHIR]));
      $years = floor($diff / (365*60*60*24));
      ?>
      <tr>
        <td><?=number_format($no)?></td>
        <td><?=$r[COL_NMNAMA]?></td>
        <td class="nik"><?=$r[COL_NONIK]?></td>
        <td class="nik"><?=$r[COL_NOKK]?></td>
        <td><?=(!empty($r[COL_NMTEMPATLAHIR])?$r[COL_NMTEMPATLAHIR]:'-').' / '.date('d-m-Y', strtotime($r[COL_TGLLAHIR]))?></td>
        <td><?=$r[COL_NMJENISKELAMIN]?></td>
        <td><?=$years?></td>
        <td><?=$r[COL_NMPENDIDIKAN]?></td>
        <td><?=$r[COL_NMDESA]?></td>
        <td><?=$r[COL_NMALAMAT]?></td>
        <?php
        /*foreach($kol as $k) {
          $val = $r[$arrnmkol[$k]];
          if($k=='TTL') $val = (!empty($r[COL_NMTEMPATLAHIR])?$r[COL_NMTEMPATLAHIR]:'-').' / '.date('d-m-Y', strtotime($r[COL_TGLLAHIR]));
          if($k=='US') {
            $diff = abs(strtotime(date('Y-m-d')) - strtotime($r[COL_TGLLAHIR]));
            $years = floor($diff / (365*60*60*24));
            $val = $years;
          }
          ?>
          <td <?=$k=='KK'?'class="nik"':''?>><?=$val?></td>
          <?php
        }*/
        foreach($kdKolom as $k) {
          ?>
          <td><?=$r['kol_'.$k]?></th>
          <?php
        }
        ?>
      </tr>
      <?php
      $no++;
    }
    ?>
  </tbody>

</table>
