<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
          <li class="breadcrumb-item active">Laporan</li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal','method'=>'get'))?>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group row">
                    <label class="control-label col-sm-4">KATEGORI</label>
                    <div class="col-sm-8">
                      <select class="form-control" name="<?=COL_KDKATEGORI?>">
                        <?=GetCombobox("SELECT * from mkategori order by Seq", COL_KDKATEGORI, COL_NMKATEGORI, (!empty($_GET[COL_KDKATEGORI])?$_GET[COL_KDKATEGORI]:null), true , false, '-- Pilih Kategori --')?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-4">KECAMATAN</label>
                    <div class="col-sm-8">
                      <select class="form-control" name="<?=COL_KDKECAMATAN?>">
                        <?=GetCombobox("SELECT * from mkecamatan order by NmKecamatan", COL_KDKECAMATAN, COL_NMKECAMATAN, (!empty($_GET[COL_KDKECAMATAN])?$_GET[COL_KDKECAMATAN]:null), true, false, '-- Pilih Kecamatan --')?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-4">DESA</label>
                    <div class="col-sm-8">
                      <select class="form-control" name="<?=COL_KDDESA?>">
                        <?php
                        if(!empty($_GET[COL_KDKECAMATAN])) {
                          echo GetCombobox("SELECT * from mdesa where KdKecamatan = ".$_GET[COL_KDKECAMATAN]." order by NmDesa", COL_KDDESA, COL_NMDESA, (!empty($_GET[COL_KDDESA])?$_GET[COL_KDDESA]:null), true, false, '-- Pilih Desa --');
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group row">
                    <label class="control-label col-sm-4">KOLOM UTAMA</label>
                    <div class="col-sm-8">
                      <select class="form-control" name="Kolom[]" multiple>
                        <option value="KK" <?=!empty($_GET['Kolom'])&&in_array('KK', $_GET['Kolom'], true)?'selected':''?>>NO. KK</option>
                        <option value="TTL" <?=!empty($_GET['Kolom'])&&in_array('TTL', $_GET['Kolom'], true)?'selected':''?>>TEMPAT & TGL. LAHIR</option>
                        <option value="ALM" <?=!empty($_GET['Kolom'])&&in_array('ALM', $_GET['Kolom'], true)?'selected':''?>>ALAMAT</option>
                        <option value="JK" <?=!empty($_GET['Kolom'])&&in_array('JK', $_GET['Kolom'], true)?'selected':''?>>JENIS KELAMIN</option>
                        <option value="US" <?=!empty($_GET['Kolom'])&&in_array('US', $_GET['Kolom'], true)?'selected':''?>>USIA</option>
                        <option value="PEN" <?=!empty($_GET['Kolom'])&&in_array('PEN', $_GET['Kolom'], true)?'selected':''?>>PENDIDIKAN</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-4">KOLOM DETAIL</label>
                    <div class="col-sm-8">
                      <select class="form-control" name="KdKolom[]" multiple>
                        <?php
                        if(!empty($_GET[COL_KDKATEGORI])) {
                          echo GetCombobox("SELECT * from mkolom where KdKategori = ".$_GET[COL_KDKATEGORI]." order by KdParent, Seq", COL_KDKOLOM, COL_NMKOLOM, (!empty($_GET['KdKolom'])?$_GET['KdKolom']:null));
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <div class="row">
                <div class="col-sm-12" style="text-align: center">
                    <button type="submit" class="btn btn-sm btn-outline-success" title="Cetak" name="cetak" value="1"><i class="fa fa-print"></i> CETAK</button>
                    <button type="submit" class="btn btn-sm btn-outline-primary" title="Lihat"><i class="fa fa-arrow-circle-right"></i> TAMPILKAN</button>
                </div>
              </div>
            </div>
            <?=form_close()?>
          </div>
      </div>
    </div>
    <?php
    if(!empty($res)) {
      ?>
      <div class="row">
        <div class="col-sm-12">
            <div class="card card-default">
              <div class="card-body">
                <?php
                  $this->load->view('admin/report/rekap-kategori_');
                ?>
              </div>
            </div>
        </div>
      </div>
      <?php
    }
    ?>
  </div>
</section>
<script>
$(document).ready(function() {
  $('[name=KdKecamatan]').change(function(){
    var kdkec = $(this).val();
    $('[name=KdDesa]').load('<?=site_url('admin/master/get-opt-desa')?>', {KdKec: kdkec}, function(){

    });
  });

  $('[name=KdKategori]').change(function(){
    var kdkat = $(this).val();
    $('select[name^=KdKolom]').load('<?=site_url('admin/master/get-kategori-opt')?>', {KdKategori: kdkat}, function(){
      $("select[name^=FormValue]", $('#card-form-detail')).trigger('change');
    });
  });
});
</script>
