<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
          <li class="breadcrumb-item active">Laporan</li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal','method'=>'get'))?>
            <div class="card-body">
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group row">
                    <label class="control-label col-sm-4">KECAMATAN</label>
                    <div class="col-sm-4">
                      <select class="form-control" name="<?=COL_KDKECAMATAN?>">
                        <?=GetCombobox("SELECT * from mkecamatan order by NmKecamatan", COL_KDKECAMATAN, COL_NMKECAMATAN, (!empty($_GET[COL_KDKECAMATAN])?$_GET[COL_KDKECAMATAN]:null), true, false, '-- Pilih Kecamatan --')?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <div class="row">
                <div class="col-sm-12" style="text-align: center">
                    <button type="submit" class="btn btn-sm btn-outline-success" title="Cetak" name="cetak" value="1"><i class="fa fa-print"></i> CETAK</button>
                    <button type="submit" class="btn btn-sm btn-outline-primary" title="Lihat"><i class="fa fa-arrow-circle-right"></i> TAMPILKAN</button>
                </div>
              </div>
            </div>
            <?=form_close()?>
          </div>
      </div>
    </div>
    <?php
    if(!empty($res)) {
      ?>
      <div class="row">
        <div class="col-sm-12">
            <div class="card card-default">
              <div class="card-body">
                <?php
                  $this->load->view('admin/report/rekap-kecamatan_');
                ?>
              </div>
            </div>
        </div>
      </div>
      <?php
    }
    ?>
  </div>
</section>
