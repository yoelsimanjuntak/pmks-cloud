<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>"><i class="fa fa-tachometer-alt"></i> Dashboard</a></li>
          <li class="breadcrumb-item active">Laporan</li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal','method'=>'get'))?>
            <div class="card-header">
              <div class="row">
                <div class="col-sm-12" style="text-align: right">
                    <button type="submit" class="btn btn-sm btn-outline-success" title="Cetak" name="cetak" value="1"><i class="fa fa-print"></i> CETAK</button>
                </div>
              </div>
            </div>
            <div class="card-body">
              <?php
              if(!empty($res)) {
                $this->load->view('admin/report/rekap-kabupaten_');
              }
              ?>
            </div>
            <?=form_close()?>
          </div>
      </div>
    </div>
  </div>
</section>
