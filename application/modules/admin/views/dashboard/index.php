<?php
$qKecamatan = @"
select
mkecamatan.NmKecamatan,
mkecamatan.NmColor,
(select count(*) from tdata dat where dat.KdKecamatan = mkecamatan.KdKecamatan) as count
from mkecamatan
order by mkecamatan.NmKecamatan
";
$rkecamatan = $this->db
->query($qKecamatan)
->result_array();

$qKategori = @"
select
mkategori.NmKategori,
(select count(*) from tdata dat where dat.KdKategori = mkategori.KdKategori) as count
from mkategori
order by mkategori.Seq
";
$rkategori = $this->db
->query($qKategori)
->result_array();

$arrDat = array(
  'label'=>array(),
  'data'=>array(),
  'color'=>array(),
);

foreach($rkecamatan as $r) {
  $arrDat['label'][] = $r[COL_NMKECAMATAN];
  $arrDat['data'][] = $r['count'];
  $arrDat['color'][] = $r[COL_NMCOLOR];
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?= $title ?></h1>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-4">
        <div class="card card-outline-purple">
          <div class="card-header">
            <h3 class="card-title font-weight-light">AKUMULASI PER KECAMATAN</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <div class="card-body">
            <canvas id="donutChart" style="min-height:320px;"></canvas>
          </div>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="card card-outline-purple">
          <div class="card-header">
            <h3 class="card-title font-weight-light">AKUMULASI PER KATEGORI</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
            </div>
          </div>
          <div class="card-body p-0">
            <table class="table table-striped">
              <tbody>
                <tr>
                  <th>No.</th>
                  <th>Kategori</th>
                  <th>Jumlah</th>
                </tr>
                <?php
                $no = 1;
                $sum = 0;
                foreach($rkategori as $r) {
                  ?>
                  <tr>
                    <td style="padding: .3rem .75rem !important" class="text-center"><?=$no?>.</td>
                    <td style="padding: .3rem .75rem !important; white-space: nowrap"><?=$r[COL_NMKATEGORI]?></td>
                    <td style="padding: .3rem .75rem !important" class="text-right"><?=number_format($r['count'])?></td>
                  </tr>
                  <?php
                  $no++;
                  $sum += $r['count'];
                }
                ?>
                <tr>
                  <td style="padding: .3rem .75rem !important" class="text-center font-weight-bold" colspan="2">TOTAL</td>
                  <td style="padding: .3rem .75rem !important" class="text-right font-weight-bold"><?=number_format($sum)?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script src="<?=base_url()?>assets/themes/adminlte-new/plugins/chart.js/Chart.min.js"></script>
<script>
var donutChartCanvas = $('#donutChart').get(0).getContext('2d');
var donutData = {
  labels: <?=json_encode($arrDat['label'])?>,
  datasets: [
    {
      data: <?=json_encode($arrDat['data'])?>,
      backgroundColor : <?=json_encode($arrDat['color'])?>,
    }
  ]
};
var donutOptions = {
  maintainAspectRatio : false,
  responsive : true,
};
var donutChart = new Chart(donutChartCanvas, {
  type: 'doughnut',
  data: donutData,
  options: donutOptions
});
</script>
