<?php
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
      '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_KDKATEGORI ] . '" />',
      $d[COL_SEQ],
      anchor('admin/master/edit-kategori/'.$d[COL_KDKATEGORI],$d[COL_NMKATEGORI],array('class' => 'modal-popup-edit', 'data-name' => $d[COL_NMKATEGORI], 'data-seq' => $d[COL_SEQ]))
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <p class="card-title">
              <?=anchor('admin/master/delete-kategori','<i class="fa fa-trash"></i> HAPUS',array('class'=>'cekboxaction btn btn-danger btn-xs','confirm'=>'Apa anda yakin?'))?>
              <?=anchor('admin/master/add-kategori','<i class="fa fa-plus"></i> TAMBAH',array('class'=>'modal-popup btn btn-primary btn-xs'))?>
            </p>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover">

              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-editor" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title font-weight-light">Kategori</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <form id="form-editor" method="post" action="#">
          <div class="form-group row">
            <div class="col-sm-8">
              <label>Nama Kategori</label>
              <div class="input-group">
                <input type="text" class="form-control" name="<?=COL_NMKATEGORI?>" />
              </div>
            </div>
            <div class="col-sm-4">
              <label>No</label>
              <div class="input-group">
                <input type="number" class="form-control" name="<?=COL_SEQ?>" />
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-xs btn-default" data-dismiss="modal"><i class="far fa-times"></i>&nbsp;CANCEL</button>
        <button type="button" class="btn btn-xs btn-primary btn-ok"><i class="far fa-check"></i>&nbsp;SIMPAN</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    var dataTable = $('#datalist').dataTable({
      "autoWidth" : false,
      //"sDom": "Rlfrtip",
      "aaData": <?=$data?>,
      //"bJQueryUI": true,
      //"aaSorting" : [[5,'desc']],
      "scrollY" : '40vh',
      "scrollX": "120%",
      "iDisplayLength": 100,
      "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
      //"dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
      "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
      "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
      "order": [[ 1, "asc" ]],
      "aoColumns": [
          {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" />", "width": "10px","bSortable":false},
          {"sTitle": "No"},
          {"sTitle": "Kategori"}
      ]
    });
    $('#cekbox').click(function(){
      if($(this).is(':checked')){
        $('.cekbox').prop('checked',true);
      }else{
        $('.cekbox').prop('checked',false);
      }
    });

    $('.modal-popup, .modal-popup-edit').click(function(){
      var a = $(this);
      var name = $(this).data('name');
      var seq = $(this).data('seq');
      var editor = $("#modal-editor");

      $('[name=<?=COL_NMKATEGORI?>]', editor).val(name);
      $('[name=<?=COL_SEQ?>]', editor).val(seq);
      editor.modal("show");
      $(".btn-ok", editor).unbind('click').click(function() {
        var dis = $(this);
        dis.html("Loading...").attr("disabled", true);
        $('#form-editor').ajaxSubmit({
          dataType: 'json',
          url : a.attr('href'),
          success : function(data){
            if(data.error==0){
              window.location.reload();
            }else{
              toastr.error(data.error);
            }
          },
          complete: function() {
            dis.html("Submit").attr("disabled", false);
          }
        });
      });
      return false;
    });
});
</script>
