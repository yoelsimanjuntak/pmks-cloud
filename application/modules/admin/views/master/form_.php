<?php
foreach($rkolom as $kol) {
  $req = $kol[COL_ISALLOWEMPTY]?'required':'';
  $val = "";
  if(!empty($KdDat)) {
    $rdat = $this->db
    ->where(TBL_TDATA_DETAIL.'.'.COL_KDDATA, $KdDat)
    ->where(TBL_TDATA_DETAIL.'.'.COL_KDKOLOM, $kol[COL_KDKOLOM])
    ->get(TBL_TDATA_DETAIL)
    ->row_array();
    if(!empty($rdat)) {
      $val = $rdat[COL_NMVALUE];
    }
  }
  ?>
  <div class="form-group row">
    <label><?=$kol[COL_NMKOLOM]?></label>
    <input type="hidden" name="FormKolom[<?=$kol[COL_KDKOLOM]?>]" />
    <?php
    if($kol[COL_NMTIPE] == "SELECT") {
      $opt = json_decode($kol[COL_NMOPTION]);
      ?>
      <select class="form-control" name="FormValue[<?=$kol[COL_KDKOLOM]?>]" data-kol="<?=$kol[COL_KDKOLOM]?>" data-dat="<?=!empty($KdDat)?$KdDat:''?>" <?=$req?>>
        <?php
        foreach($opt as $o) {
          echo '<option value="'.$o.'" '.(!empty($val)&&$val==$o?'selected':'').'>'.$o.'</option>';
        }
        ?>
      </select>
      <?php
    } else {
      ?>
      <input type="text" class="form-control" name="FormValue[<?=$kol[COL_KDKOLOM]?>]" data-kol="<?=$kol[COL_KDKOLOM]?>" data-dat="<?=!empty($KdDat)?$KdDat:''?>" value="<?=!empty($val)?$val:''?>" <?=$req?> />
      <?php
    }
    ?>
  </div>
  <div class="form-group-after">

  </div>
  <?php
}
?>
<script>
$(document).ready(function() {
  $("select[name^=FormValue]").not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
  $("select[name^=FormValue]").change(function() {
    var dis = $(this);
    var nextel = dis.closest('.form-group').next('.form-group-after');
    var kdkol = dis.data('kol');
    var kddat = dis.data('dat');
    var nmcond = dis.val();
    nextel.load('<?=site_url('admin/master/get-kategori-child')?>', {KdKolom: kdkol, KdCond: nmcond, KdData: kddat}, function(){
      $("select[name^=FormValue]", nextel).trigger('change');
    });
  });/*.trigger('change');*/
});
</script>
