<?php
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
      '<input type="checkbox" class="cekbox" name="cekbox[]" value="' . $d[COL_KDKOLOM] . '" />',
      $d['SeqKategori'].'. '.$d[COL_NMKATEGORI],
      anchor('admin/master/edit-kolom/'.$d[COL_KDKOLOM],$d[COL_SEQ].'. '.$d[COL_NMKOLOM])
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <p class="card-title">
              <?=anchor('admin/master/delete-kolom','<i class="fa fa-trash"></i> HAPUS',array('class'=>'cekboxaction btn btn-danger btn-xs','confirm'=>'Apa anda yakin?'))?>
              <?=anchor('admin/master/add-kolom','<i class="fa fa-plus"></i> TAMBAH',array('class'=>'modal-popup btn btn-primary btn-xs'))?>
            </p>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover">

              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
    var dataTable = $('#datalist').dataTable({
      "autoWidth" : false,
      //"sDom": "Rlfrtip",
      "aaData": <?=$data?>,
      //"bJQueryUI": true,
      //"aaSorting" : [[5,'desc']],
      "scrollY" : '40vh',
      "scrollX": "120%",
      "iDisplayLength": 100,
      "aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
      //"dom":"R<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
      "dom":"R<'row'<'col-sm-4'l><'col-sm-8'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
      "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
      "order": [[ 1, "asc" ]],
      "aoColumns": [
          {"sTitle": "<input type=\"checkbox\" id=\"cekbox\" />", "width": "10px","bSortable":false},
          {"sTitle": "Kategori"},
          {"sTitle": "Kolom"}
      ]
    });
    $('#cekbox').click(function(){
      if($(this).is(':checked')){
        $('.cekbox').prop('checked',true);
      }else{
        $('.cekbox').prop('checked',false);
      }
    });
});
</script>
