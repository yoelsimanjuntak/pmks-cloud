<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?= $title ?></h3>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url('admin/dashboard')?>"><i class="fa fa-home"></i> Home</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('admin/master/index-kolom')?>">Kolom</a></li>
          <li class="breadcrumb-item active">Form</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open(current_url(),array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <a href="<?=site_url('admin/master/index-kolom')?>" class="btn btn-xs btn-secondary"><i class="far fa-arrow-left"></i>&nbsp;KEMBALI</a>
          <button type="submit" class="btn btn-xs btn-success"><i class="far fa-check"></i>&nbsp;SIMPAN</button>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-purple">
          <div class="card-header">
            <h5 class="card-title font-weight-light">Informasi Kolom</h5>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label>Kategori</label>
                  <select class="form-control" name="<?=COL_KDKATEGORI?>">
                    <?=GetCombobox("SELECT * from mkategori order by Seq", COL_KDKATEGORI, COL_NMKATEGORI, (!empty($data)?$data[COL_KDKATEGORI]:null))?>
                  </select>
                </div>
                <div class="form-group">
                  <label>No. Urut</label>
                  <input type="number" class="form-control" name="<?=COL_SEQ?>" placeholder="No." value="<?=!empty($data)?$data[COL_SEQ]:''?>" required />
                </div>
                <div class="form-group">
                  <label>Nama Kolom</label>
                  <input type="text" class="form-control" name="<?=COL_NMKOLOM?>" placeholder="Nama Kolom" value="<?=!empty($data)?$data[COL_NMKOLOM]:''?>" required />
                </div>
                <div class="form-group">
                  <label>Jenis Kolom</label>
                  <select class="form-control" name="<?=COL_NMTIPE?>">
                    <option value="TEXT" <?=!empty($data)&&$data[COL_NMTIPE]=='TEXT'?'selected':''?>>TEXT</option>
                    <option value="SELECT" <?=!empty($data)&&$data[COL_NMTIPE]=='SELECT'?'selected':''?>>SELECT</option>
                  </select>
                </div>
                <div class="form-group div-opsi">
                  <input type="hidden" name="<?=COL_NMOPTION?>" />
                  <table id="tbl-opsi" class="table table-bordered">
                    <thead>
                      <tr>
                        <th>Opsi</th>
                        <th class="text-center" style="width: 10px; white-space: nowrap">#</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                    <tfoot>
                      <tr>
                        <th class="text-center" style="white-space: nowrap">
                          <input type="text" class="form-control form-control-sm" name="OpsiText" placeholder="Opsi" />
                        </th>
                        <th class="text-center" style="width: 10px; white-space: nowrap">
                          <button type="button" id="btn-add-opsi" class="btn btn-sm btn-outline-success"><i class="fa fa-plus"></i></button>
                        </th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <div class="form-group">
                  <label><input type="checkbox" name="<?=COL_ISALLOWEMPTY?>" />&nbsp;BOLEH KOSONG</label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <?php
        if(!empty($data) && !empty($data[COL_KDPARENT])) {
          $rparent = $this->db
          ->where(COL_KDKOLOM, $data[COL_KDPARENT])
          ->get(TBL_MKOLOM)
          ->row_array();
          if(!empty($rparent)) {
            $arrParentOpt = json_decode($rparent[COL_NMOPTION]);
            ?>
            <div class="card card-outline card-purple">
              <div class="card-header">
                <h5 class="card-title font-weight-light">Kolom Referensi</h5>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="form-group">
                      <label>Nama Kolom</label>
                      <input type="text" class="form-control" placeholder="Nama Kolom" value="<?=$rparent[COL_NMKOLOM]?>" disabled />
                    </div>
                    <div class="form-group">
                      <label>Kondisi</label>
                      <select class="form-control" name="<?=COL_KDCONDITION?>">
                        <?php
                        foreach($arrParentOpt as $opt) {
                          ?>
                          <option value="<?=$opt?>" <?=!empty($data)&&$data[COL_KDCONDITION]==$opt?'selected':''?>><?=$opt?></option>
                          <?php
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php
          }
        }
        ?>
        <div class="card card-outline card-purple">
          <div class="card-header">
            <h5 class="card-title font-weight-light">Kolom Turunan</h5>
          </div>
          <div class="card-body">
            <input type="hidden" name="ArrChild" />
            <table id="tbl-child" class="table table-bordered">
              <thead>
                <tr>
                  <th>No. & Nama</th>
                  <th>Kondisi</th>
                  <th>Opsi</th>
                  <th class="text-center" style="width: 10px; white-space: nowrap">
                    <button type="button" id="btn-add-child" class="btn btn-sm btn-outline-success"><i class="fa fa-plus"></i></button>
                  </th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <?=form_close()?>
  </div>
</section>
<div class="modal fade" id="modal-child" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Kolom Turunan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <label>Kondisi</label>
          <select name="ChildCond" class="form-control" style="width: 100%"></select>
        </div>
        <div class="form-group row">
          <div class="col-sm-4">
            <label>No.</label>
            <input type="number" name="ChildNo" class="form-control" />
          </div>
          <div class="col-sm-8">
            <label>Nama</label>
            <input type="text" name="ChildName" class="form-control" />
          </div>
        </div>
        <div class="form-group">
          <label>Opsi <small class="font-italic">Pisah menggunakan "<strong>,</strong>"</small></label>
          <input type="text" name="ChildOpt" class="form-control" />
        </div>
      </div>
      <div class="modal-footer text-center">
        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fas fa-times"></i>&nbsp;BATAL</button>&nbsp;
        <button type="button" class="btn btn-primary btn-ok"><i class="fas fa-check"></i>&nbsp;TAMBAH</button>
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function() {
  var modalc = $('#modal-child');
  $('.modal').on('hidden.bs.modal', function (event) {
    $('input, select', $(this)).val('');
  });

  $('[name=NmTipe]').change(function(){
    var sel = $(this).val();
    if(sel == 'SELECT') $('.div-opsi').show();
    else $('.div-opsi').hide();
  }).trigger('change');

  $('#form-main').validate({
    ignore: "[type=file]",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      btnSubmit.attr('disabled', true);
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success('Berhasil');
            if(res.data.redirect) {
              location.href = res.data.redirect;
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });

  $('[name=NmOption]').change(function() {
    writeOpsi('tbl-opsi', 'NmOption');
  }).val(encodeURIComponent('<?=$def['NmOption']?>')).trigger('change');
  $('[name=ArrChild]').change(function() {
    writeChild('tbl-child', 'ArrChild');
  }).val(encodeURIComponent('<?=$def['ArrChild']?>')).trigger('change');

  $('#btn-add-opsi', $('#tbl-opsi')).click(function() {
    var dis = $(this);
    var arr = $('[name=NmOption]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var row = dis.closest('tr');
    var opt = $('[name=OpsiText]', row).val();
    if(opt) {
      var exist = jQuery.grep(arr, function(a) {
        return a == opt;
      });
      if(exist.length == 0) {
        arr.push(opt);
        $('[name=NmOption]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
        $('input', row).val('');
      }
    }
  });

  $('#btn-add-child', '#tbl-child').click(function(){
    $('[name=ChildCond]').empty();
    var mainopt = $('[name=NmOption]').val();
    if(mainopt) {
      mainopt = JSON.parse(decodeURIComponent(mainopt));
      if(mainopt.length > 0) {
        var opts = '';
        for (var i=0; i<mainopt.length; i++) {
          opts += '<option value="'+mainopt[i]+'">'+mainopt[i]+'</option>';
        }
        $('[name=ChildCond]').html(opts);
      }
    }
    modalc.modal('show');
  });

  $('.btn-ok', modalc).click(function(){
    var arr = $('[name=ArrChild]').val();
    if(arr) arr = JSON.parse(decodeURIComponent(arr));
    else arr = [];

    var ChildNo = $('[name=ChildNo]', modalc).val();
    var ChildName = $('[name=ChildName]', modalc).val();
    var ChildCond = $('[name=ChildCond]', modalc).val();
    var ChildOpt = $('[name=ChildOpt]', modalc).val();
    if(ChildNo && ChildName && ChildCond && ChildOpt) {
      arr.push({"ChildNo":ChildNo, "ChildName":ChildName, "ChildCond":ChildCond, "ChildOpt":ChildOpt});
      $('[name=ArrChild]').val(encodeURIComponent(JSON.stringify(arr))).trigger('change');
      $('select', modalc).empty();
      modalc.modal('hide');
    } else {
      toastr.error('Isian tidak valid');
    }
  });
});

function writeOpsi(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      /*arr = arr.sort(function (a, b) {
        return a.localeCompare(b);
      });*/
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i]+'</td>';
        html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i]+'" /></td>';
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val();
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){ return e != idx; });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="2"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="2"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}

function writeChild(tbl, input) {
  var tbl = $('#'+tbl+'>tbody');
  var arr = $('[name='+input+']').val();
  if(arr) {
    arr = JSON.parse(decodeURIComponent(arr));
    if(arr.length > 0) {
      arr = arr.sort(function (a, b) {
        return a['ChildCond'].localeCompare(b['ChildCond']);
      });
      var html = '';
      for (var i=0; i<arr.length; i++) {
        html += '<tr>';
        html += '<td>'+arr[i].ChildNo+'. '+arr[i].ChildName+'</td>';
        html += '<td>'+arr[i].ChildCond+'</td>';
        html += '<td>'+arr[i].ChildOpt+'</td>';
        html += '<td class="text-center"><button type="button" class="btn btn-sm btn-outline-danger btn-del"><i class="fas fa-minus"></i></button><input type="hidden" name="idx" value="'+arr[i].ChildCond+'@@'+arr[i].ChildNo+'@@'+arr[i].ChildName+'" /></td>';
        html += '</tr>';
      }
      tbl.html(html);

      $('.btn-del', tbl).click(function() {
        var row = $(this).closest('tr');
        var idx = $('[name=idx]', row).val().split('@@');
        if(idx) {
          var arr = $('[name='+input+']').val();
          arr = JSON.parse(decodeURIComponent(arr));

          var arrNew = $.grep(arr, function(e){
            return e.ChildCond != idx[0] || e.ChildNo != idx[1] || e.ChildName != idx[2];
          });
          $('[name='+input+']').val(encodeURIComponent(JSON.stringify(arrNew))).trigger('change');
        }
      });
    } else {
      tbl.html('<tr><td colspan="4"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
    }
  } else {
    tbl.html('<tr><td colspan="4"><p class="font-italic text-center m-0">Belum ada data</p></td></tr>');
  }
}
</script>
