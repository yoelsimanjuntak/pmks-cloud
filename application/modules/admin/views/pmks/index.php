<?php
$data = array();
$i = 0;
foreach ($res as $d) {
    $res[$i] = array(
      '<input type="checkbox" class="cekbox" name="cekbox[]" value="'.$d[COL_UNIQ].'" />',
      anchor('admin/pmks/edit/'.$d[COL_UNIQ],$d[COL_NONIK]),
      $d[COL_NMKATEGORI],
      $d[COL_NOKK],
      $d[COL_NMNAMA],
      $d[COL_NMKECAMATAN],
      $d[COL_NMDESA]
    );
    $i++;
}
$data = json_encode($res);
$user = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <h1 class="m-0 font-weight-light"><?= $title ?></h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Home</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <p class="card-title">
              <?=anchor('admin/pmks/delete','<i class="fa fa-trash"></i> HAPUS',array('class'=>'cekboxaction btn btn-danger btn-xs','confirm'=>'Apa anda yakin?'))?>
              <?=anchor('admin/pmks/add','<i class="fa fa-plus"></i> TAMBAH',array('class'=>'modal-popup btn btn-primary btn-xs'))?>
            </p>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered table-hover table-condensed">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">#</th>
                    <th>NIK</th>
                    <th>KATEGORI</th>
                    <th>NO. KK</th>
                    <th>NAMA</th>
                    <th>KECAMATAN</th>
                    <th>DESA</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div id="dom-filter" class="d-none">
  <div class="form-group">
    <select class="form-control" name="filterIdKategori">
      <?=GetCombobox("SELECT * from mkategori order by Seq", COL_KDKATEGORI, COL_NMKATEGORI, null, true , false, '-- SEMUA KATEGORI --')?>
    </select>
  </div>
  <div class="form-group">
    <div class="row">
      <div class="col-sm-6">
        <select class="form-control" name="filterIdKecamatan">
          <?=GetCombobox("SELECT * from mkecamatan order by NmKecamatan", COL_KDKECAMATAN, COL_NMKECAMATAN, null, true , false, '-- SEMUA KECAMATAN --')?>
        </select>
      </div>
      <div class="col-sm-6">
        <select class="form-control" name="filterIdDesa">
          <option value="">-- SEMUA DESA / KELURAHAN --</option>
        </select>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('admin/pmks/index-load')?>",
      "type": 'POST',
      "data": function(data){
          var idKategori = $('[name=filterIdKategori]', $('.domfilter')).val();
          var idKecamatan = $('[name=filterIdKecamatan]', $('.domfilter')).val();
          var idDesa = $('[name=filterIdDesa]', $('.domfilter')).val();

          data.idKategori = idKategori;
          data.idKecamatan = idKecamatan;
          data.idDesa = idDesa;
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "dom":"R<'row'<'col-sm-8 domfilter'><'col-sm-4 text-right'f<'clear'>B>><'row'<'col-sm-12'tr>><'row mt-2'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    "order": [[ 1, "asc" ]],
    "columnDefs": [{"targets":[0,1,2], "className":'nowrap'}],
    "columns": [
      {"orderable": false},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true}
    ]
  });
  $("div.domfilter").html($('#dom-filter').html());
  $('input,select', $("div.domfilter")).change(function() {
    dt.DataTable().ajax.reload();
  });

  $('[name=filterIdKecamatan]').change(function(){
    var kdkec = $(this).val();
    $('[name=filterIdDesa]').load('<?=site_url('admin/master/get-opt-desa')?>', {KdKec: kdkec, emptyText: '-- SEMUA DESA / KELURAHAN --'}, function(){
      $('[name=filterIdDesa]').trigger('change');
    });
  });

  $('#cekbox').click(function(){
    if($(this).is(':checked')){
      $('.cekbox').prop('checked',true);
    }else{
      $('.cekbox').prop('checked',false);
    }
  });
});
</script>
