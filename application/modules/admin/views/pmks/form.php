<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?= $title ?></h3>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url('admin/dashboard')?>"><i class="fa fa-home"></i> Home</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('admin/pmks/index')?>">Data PMKS</a></li>
          <li class="breadcrumb-item active">Form</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open(current_url(),array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <a href="<?=site_url('admin/pmks/index')?>" class="btn btn-xs btn-secondary"><i class="far fa-arrow-left"></i>&nbsp;KEMBALI</a>
          <button type="submit" class="btn btn-xs btn-success"><i class="far fa-check"></i>&nbsp;SIMPAN</button>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-purple">
          <div class="card-header">
            <h5 class="card-title font-weight-light">Identitas</h5>
          </div>
          <div class="card-body">
            <?php
            if(!empty($data[COL_NMFOTO])) {
              ?>
              <div class="text-center mb-2">
                <img class="profile-user-img img-fluid img-circle" src="<?=MY_UPLOADURL.$data[COL_NMFOTO]?>" alt="Profile">
              </div>
              <?php
            }
            ?>

            <div class="form-group row">
              <label class="control-label col-sm-3">Foto</label>
              <div class="col-sm-9">
                <div class="input-group">
                  <div class="custom-file">
                    <input id="upload" type="file" class="custom-file-input" name="<?=COL_NMFOTO?>" accept="image/*" required  />
                    <label class="custom-file-label font-italic" for="upload">Upload foto</label>
                  </div>
                </div>
                <p class="text-sm font-italic text-danger mb-0">
                  Catatan: ukuran maks. 2MB dengan dimensi maks 2048 x 2048
                </p>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Kategori</label>
              <div class="col-sm-9">
                <select class="form-control" name="<?=COL_KDKATEGORI?>">
                  <?=GetCombobox("SELECT * from mkategori order by Seq", COL_KDKATEGORI, COL_NMKATEGORI, (!empty($data)?$data[COL_KDKATEGORI]:null), true , false, '-- Pilih Kategori --')?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">NIK</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="<?=COL_NONIK?>" placeholder="NIK" value="<?=!empty($data)?$data[COL_NONIK]:''?>" required />
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">No. KK</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="<?=COL_NOKK?>" placeholder="No. KK" value="<?=!empty($data)?$data[COL_NOKK]:''?>" required />
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Nama Lengkap</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="<?=COL_NMNAMA?>" placeholder="Nama Lengkap" value="<?=!empty($data)?$data[COL_NMNAMA]:''?>" required />
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Alamat</label>
              <div class="col-sm-9">
                <textarea rows="3" class="form-control" name="<?=COL_NMALAMAT?>" placeholder="Alamat"><?=!empty($data)?$data[COL_NMALAMAT]:''?></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Kecamatan</label>
              <div class="col-sm-9">
                <select class="form-control" name="<?=COL_KDKECAMATAN?>">
                  <?=GetCombobox("SELECT * from mkecamatan order by NmKecamatan", COL_KDKECAMATAN, COL_NMKECAMATAN, (!empty($data)?$data[COL_KDKECAMATAN]:null), true, false, '-- Pilih Kecamatan --')?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Desa</label>
              <div class="col-sm-9">
                <select class="form-control" name="<?=COL_KDDESA?>">
                  <?php
                  if(!empty($data)) {
                    $kdKec = $data[COL_KDKECAMATAN];
                    echo GetCombobox("SELECT * from mdesa where KdKecamatan = $kdKec order by NmDesa", COL_KDDESA, COL_NMDESA, (!empty($data)?$data[COL_KDDESA]:null));
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Tempat Lahir</label>
              <div class="col-sm-9">
                <textarea rows="3" class="form-control" name="<?=COL_NMTEMPATLAHIR?>" placeholder="Alamat"><?=!empty($data)?$data[COL_NMTEMPATLAHIR]:''?></textarea>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Tanggal Lahir</label>
              <div class="col-sm-3">
                <input type="text" class="form-control datepicker" name="<?=COL_TGLLAHIR?>" value="<?=!empty($data)?$data[COL_TGLLAHIR]:''?>" required />
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Jenis Kelamin</label>
              <div class="col-sm-3">
                <div class="form-check">
                  <input id="RadioGenderLK" class="form-check-input" type="radio" name="<?=COL_NMJENISKELAMIN?>" value="LAKI-LAKI" <?=empty($data[COL_NMJENISKELAMIN])?"checked":($data[COL_NMJENISKELAMIN]=="LAKI-LAKI"?"checked":"")?>>
                  <label class="form-check-label" for="RadioGenderLK">LAKI-LAKI</label>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-check">
                  <input id="RadioGenderPR" class="form-check-input" type="radio" name="<?=COL_NMJENISKELAMIN?>" value="PEREMPUAN" <?=empty($data[COL_NMJENISKELAMIN])?"":($data[COL_NMJENISKELAMIN]=="PEREMPUAN"?"checked":"")?>>
                  <label class="form-check-label" for="RadioGenderPR">PEREMPUAN</label>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Nama Ayah</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="<?=COL_NMAYAH?>" value="<?=!empty($data)?$data[COL_NMAYAH]:''?>" required />
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Nama Ibu</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" name="<?=COL_NMIBU?>" value="<?=!empty($data)?$data[COL_NMIBU]:''?>" required />
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Pendidikan</label>
              <div class="col-sm-9">
                <select class="form-control" name=<?=COL_NMPENDIDIKAN?>>
                  <option value="Tidak Sekolah" <?=!empty($data)&&$data[COL_NMPENDIDIKAN]=='Tidak Sekolah'?'selected':''?>>Tidak Sekolah</option>
                  <option value="Paud / TK" <?=!empty($data)&&$data[COL_NMPENDIDIKAN]=='Paud / TK'?'selected':''?>>Paud / TK</option>
                  <option value="SD / Sederajat" <?=!empty($data)&&$data[COL_NMPENDIDIKAN]=='SD / Sederajat'?'selected':''?>>SD / Sederajat</option>
                  <option value="SMP / Sederajat" <?=!empty($data)&&$data[COL_NMPENDIDIKAN]=='SMP / Sederajat'?'selected':''?>>SMP / Sederajat</option>
                  <option value="SMA / SMK / Sederajat" <?=!empty($data)&&$data[COL_NMPENDIDIKAN]=='SMA / SMK / Sederajat'?'selected':''?>>SMA / SMK / Sederajat</option>
                  <option value="D3" <?=!empty($data)&&$data[COL_NMPENDIDIKAN]=='D3'?'selected':''?>>D3</option>
                  <option value="S1" <?=!empty($data)&&$data[COL_NMPENDIDIKAN]=='S1'?'selected':''?>>S1</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-3">Rekan Tinggal</label>
              <div class="col-sm-9">
                <select class="form-control" name=<?=COL_NMREKANTINGGAL?>>
                  <option value="Orangtua Kandung" <?=!empty($data)&&$data[COL_NMREKANTINGGAL]=='Orangtua Kandung'?'selected':''?>>Orangtua Kandung</option>
                  <option value="Famili Dekat" <?=!empty($data)&&$data[COL_NMREKANTINGGAL]=='Famili Dekat'?'selected':''?>>Famili Dekat</option>
                  <option value="Famili Lain" <?=!empty($data)&&$data[COL_NMREKANTINGGAL]=='Famili Lain'?'selected':''?>>Famili Lain</option>
                  <option value="Tidak Ada" <?=!empty($data)&&$data[COL_NMREKANTINGGAL]=='Tidak Ada'?'selected':''?>>Tidak Ada</option>
                </select>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div id="card-form-detail" class="card card-outline card-purple">
          <div class="card-header">
            <h5 class="card-title font-weight-light">Informasi Detail</h5>
          </div>
          <div class="card-body">
            <?php
            if(!empty($data)) {
              $rkolom = $this->db
              ->join(TBL_MKOLOM,TBL_MKOLOM.'.'.COL_KDKOLOM." = ".TBL_TDATA_DETAIL.".".COL_KDKOLOM,"left")
              ->where(TBL_MKOLOM.'.'.COL_KDKATEGORI, $data[COL_KDKATEGORI])
              ->where(TBL_TDATA_DETAIL.'.'.COL_KDDATA, $data[COL_UNIQ])
              ->where(TBL_MKOLOM.'.'.COL_KDPARENT, null)
              ->order_by(TBL_MKOLOM.'.'.COL_SEQ)
              ->get(TBL_TDATA_DETAIL)
              ->result_array();

              foreach($rkolom as $det) {
                $req = $det[COL_ISALLOWEMPTY]?'required':'';
                ?>
                <div class="form-group row">
                  <label><?=$det[COL_NMKOLOM]?></label>
                  <input type="hidden" name="FormKolom[<?=$det[COL_KDKOLOM]?>]" />
                  <?php
                  if($det[COL_NMTIPE] == "SELECT") {
                    $opt = json_decode($det[COL_NMOPTION]);
                    ?>
                    <select class="form-control" name="FormValue[<?=$det[COL_KDKOLOM]?>]" data-kol="<?=$det[COL_KDKOLOM]?>" data-dat="<?=$det[COL_KDDATA]?>" <?=$req?>>
                      <?php
                      foreach($opt as $o) {
                        echo '<option value="'.$o.'" '.(!empty($det)&&$det[COL_NMVALUE]==$o?'selected':'').'>'.$o.'</option>';
                      }
                      ?>
                    </select>
                    <?php
                  } else {
                    ?>
                    <input type="text" class="form-control" name="FormValue[<?=$det[COL_KDKOLOM]?>]" data-kol="<?=$det[COL_KDKOLOM]?>" data-dat="<?=$det[COL_KDDATA]?>" value="<?=!empty($det)?$det[COL_NMVALUE]:''?>" <?=$req?> />
                    <?php
                  }
                  ?>
                </div>
                <div class="form-group-after">

                </div>
                <?php
              }
            }
            ?>
          </div>
        </div>
      </div>
    </div>
    <?=form_close()?>
  </div>
</section>
<script>
$(document).ready(function() {
  $('[name=KdKecamatan]').change(function(){
    var kdkec = $(this).val();
    $('[name=KdDesa]').load('<?=site_url('admin/master/get-opt-desa')?>', {KdKec: kdkec}, function(){

    });
  });

  $("select[name^=FormValue]").change(function() {
    var dis = $(this);
    var nextel = dis.closest('.form-group').next('.form-group-after');
    var kdkol = dis.data('kol');
    var kddat = dis.data('dat');
    var nmcond = dis.val();
    nextel.load('<?=site_url('admin/master/get-kategori-child')?>', {KdKolom: kdkol, KdCond: nmcond, KdData: kddat}, function(){
      $("select[name^=FormValue]", nextel).trigger('change');
    });
  }).trigger('change');

  $('[name=KdKategori]').change(function(){
    var kdkat = $(this).val();
    $('.card-body', $('#card-form-detail')).load('<?=site_url('admin/master/get-kategori-form')?>', {KdKategori: kdkat}, function(){
      $("select[name^=FormValue]", $('#card-form-detail')).trigger('change');
    });
  });


  $('#form-main').validate({
    ignore: "[type=file]",
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', $(form));
      btnSubmit.attr('disabled', true);
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success('Berhasil');
            if(res.data.redirect) {
              location.href = res.data.redirect;
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});

</script>
