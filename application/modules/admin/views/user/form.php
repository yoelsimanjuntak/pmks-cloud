<?php
$rdriver = array();
$rbusiness = array();
$arrschedule = array();
?>
<style>
#map {
    height: 400px;  /* The height is 400 pixels */
    width: 100%;  /* The width is the width of the web page */
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 font-weight-light"><?= $title ?></h3>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url('admin/dashboard')?>"><i class="fa fa-home"></i> Home</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url('admin/user/index/'.$role)?>">Pengguna</a></li>
          <li class="breadcrumb-item active">Form</li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <?=form_open_multipart(current_url(),array('role'=>'form','id'=>'form-main','class'=>'form-horizontal'))?>
    <div class="row">
      <div class="col-sm-12">
        <div class="form-group">
          <a href="<?=site_url('admin/user/index/'.$role)?>" class="btn btn-sm btn-secondary"><i class="far fa-arrow-left"></i>&nbsp;KEMBALI</a>
          <button type="submit" class="btn btn-sm btn-success"><i class="far fa-check"></i>&nbsp;SIMPAN</button>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-olive">
          <div class="card-header">
            <p class="mb-0">
              <h5 class="card-title font-weight-light">Akun & Data Diri</h5>
            </p>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <div class="row">
                    <div class="col-sm-6">
                      <label>Email</label>
                      <input type="text" class="form-control" name="<?=COL_EMAIL?>" placeholder="Email" value="<?=!empty($data)?$data[COL_EMAIL]:''?>" <?=!empty($data)?'readonly':'required'?> />
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6">
                    <label>Nama</label>
                    <input type="text" class="form-control" name="<?=COL_NAMA?>" placeholder="Nama" value="<?=!empty($data)?$data[COL_NAMA]:''?>" required />
                  </div>
                  <div class="col-sm-6">
                    <label>Foto<?=!empty($data)&&!empty($data[COL_FOTO])?'&nbsp;<small><a href="'.MY_UPLOADURL.$data[COL_FOTO].'" target="_blank" class="btn-popup-foto" data-title="Foto">LIHAT</a></small>':''?></label>
                    <input type="file" class="d-block" name="<?=COL_FOTO?>" accept=".jpg,.png,.jpeg" />
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6">
                    <label>NIK</label>
                    <input type="text" class="form-control" name="<?=COL_NIK?>" placeholder="NIK" value="<?=!empty($data)?$data[COL_NIK]:''?>" />
                  </div>
                  <div class="col-sm-6">
                    <label>Foto KTP <?=!empty($data)&&!empty($data[COL_FOTOKTP])?'&nbsp;<small><a href="'.MY_UPLOADURL.$data[COL_FOTOKTP].'" target="_blank" class="btn-popup-foto" data-title="KTP">LIHAT</a></small>':''?></label>
                    <input type="file" class="d-block" name="<?=COL_FOTOKTP?>" accept=".jpg,.png,.jpeg" />
                  </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-6">
                    <label>Tanggal Lahir</label>
                    <input type="text" class="form-control datepicker" name="<?=COL_TANGGALLAHIR?>" placeholder="yyyy-mm-dd" value="<?=!empty($data)?$data[COL_TANGGALLAHIR]:''?>" required />
                  </div>
                  <div class="col-sm-6">
                    <label>No. Telp / HP</label>
                    <input type="text" class="form-control" name="<?=COL_NOTELP?>" placeholder="No. Telp / HP" value="<?=!empty($data)?$data[COL_NOTELP]:''?>" required />
                  </div>
                </div>
                <div class="form-group">
                  <label>Alamat</label>
                  <div class="row">
                    <div class="col-sm-12">
                      <textarea class="form-control" name="<?=COL_ALAMAT?>" placeholder="Alamat" required><?=!empty($data)?$data[COL_ALAMAT]:''?></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="card card-outline card-olive">
          <div class="card-header">
            <h5 class="card-title font-weight-light">Pengaturan Password</h5>
          </div>
          <div class="card-body">
            <div class="form-group row">
              <label class="control-label col-sm-4">Password</label>
              <div class="col-sm-8">
                <input type="password" class="form-control" name="<?=COL_PASSWORD?>" placeholder="Password" />
              </div>
            </div>
            <div class="form-group row">
              <label class="control-label col-sm-4">Konfirmasi Password</label>
              <div class="col-sm-8">
                <input type="password" class="form-control" name="ConfirmPassword" placeholder="Konfirmasi Password" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?=form_close()?>
  </div>
</section>
<div class="modal fade" id="modal-koordinat" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Koordinat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
        <p class="font-italic"><span class="text-red">*)</span> Klik pada map untuk menandai lokasi</p>
        <div id="map"></div>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-gambar" tabindex="-1" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title font-weight-light">Foto</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true"><i class="fas fa-close"></i></span>
        </button>
      </div>
      <div class="modal-body">
      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
  $('#form-main').validate({
    ignore: "[type=file]",
    submitHandler: function(form) {
      if(!confirm('Apakah anda yakin?')) {
        return false;
      }

      var btnSubmit = $('button[type=submit], button.btn-verify', $(form));
      btnSubmit.attr('disabled', true);
      $(form).ajaxSubmit({
        dataType: 'json',
        type : 'post',
        success: function(res) {
          if(res.error != 0) {
            toastr.error(res.error);
          } else {
            toastr.success('Berhasil');
            if(res.data && res.data.redirect) {
              setTimeout(function(){
                location.href = res.data.redirect;
              }, 1000);
            }
          }
        },
        error: function() {
          toastr.error('SERVER ERROR');
        },
        complete: function() {
          btnSubmit.attr('disabled', false);
        }
      });
      return false;
    }
  });
});

</script>
