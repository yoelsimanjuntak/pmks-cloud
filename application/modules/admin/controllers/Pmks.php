<?php
class Pmks extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin() ) {
      redirect('site/home');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEOPERATOR) {
      redirect('admin/dashboard/login');
    }
  }

  public function index() {
    $data['title'] = 'Data PMKS';
    $data['res'] = $this->db
    ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KDKECAMATAN." = ".TBL_TDATA.".".COL_KDKECAMATAN,"left")
    ->join(TBL_MDESA,TBL_MDESA.'.'.COL_KDDESA." = ".TBL_TDATA.".".COL_KDDESA,"left")
    ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_KDKATEGORI." = ".TBL_TDATA.".".COL_KDKATEGORI,"left")
    ->get(TBL_TDATA)
    ->result_array();
    $this->template->load('main', 'admin/pmks/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $idKategori = !empty($_POST['idKategori'])?$_POST['idKategori']:null;
    $idKecamatan = !empty($_POST['idKecamatan'])?$_POST['idKecamatan']:null;
    $idDesa = !empty($_POST['idDesa'])?$_POST['idDesa']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_NONIK=>'asc');
    $orderables = array(COL_NONIK,COL_NMKATEGORI,COL_NOKK,COL_NMNAMA,COL_NMKECAMATAN,COL_NMDESA);
    $cols = array(COL_NONIK,COL_NMKATEGORI,COL_NOKK,COL_NMNAMA,COL_NMKECAMATAN,COL_NMDESA);

    $queryAll = $this->db
    ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KDKECAMATAN." = ".TBL_TDATA.".".COL_KDKECAMATAN,"left")
    ->join(TBL_MDESA,TBL_MDESA.'.'.COL_KDDESA." = ".TBL_TDATA.".".COL_KDDESA,"left")
    ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_KDKATEGORI." = ".TBL_TDATA.".".COL_KDKATEGORI,"left")
    ->get(TBL_TDATA);

    $i = 0;
    foreach($cols as $item){
      if($item == COL_NMKATEGORI) $item = TBL_MKATEGORI.'.'.COL_NMKATEGORI;
      if($item == COL_NMKECAMATAN) $item = TBL_MKECAMATAN.'.'.COL_NMKECAMATAN;
      if($item == COL_NMDESA) $item = TBL_MDESA.'.'.COL_NMDESA;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($idKategori)) {
      $this->db->where(TBL_MKATEGORI.'.'.COL_KDKATEGORI, $idKategori);
    }
    if(!empty($idKecamatan)) {
      $this->db->where(TBL_TDATA.'.'.COL_KDKECAMATAN, $idKecamatan);
    }
    if(!empty($idDesa)) {
      $this->db->where(TBL_TDATA.'.'.COL_KDDESA, $idDesa);
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
        $order = $orderdef;
        $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KDKECAMATAN." = ".TBL_TDATA.".".COL_KDKECAMATAN,"left")
    ->join(TBL_MDESA,TBL_MDESA.'.'.COL_KDDESA." = ".TBL_TDATA.".".COL_KDDESA,"left")
    ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_KDKATEGORI." = ".TBL_TDATA.".".COL_KDKATEGORI,"left")
    ->get_compiled_select(TBL_TDATA, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $data[] = array (
        '<input type="checkbox" class="cekbox" name="cekbox[]" value="'.$r[COL_UNIQ].'" />',
        '<a href="'.site_url('admin/pmks/edit/'.$r[COL_UNIQ]).'">'.$r[COL_NONIK].'</a>',
        $r[COL_NMKATEGORI],
        $r[COL_NOKK],
        $r[COL_NMNAMA],
        $r[COL_NMKECAMATAN],
        $r[COL_NMDESA]
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function add() {
    $user = GetLoggedUser();
    if($user[COL_ROLEID] != ROLEADMIN && $user[COL_ROLEID] != ROLEOPERATOR) {
      redirect('admin/dashboard/login');
    }

    $data['title'] = 'Data PMKS';
    if(!empty($_POST)) {
      $data = array(
        COL_KDKATEGORI => $this->input->post(COL_KDKATEGORI),
        COL_NONIK => $this->input->post(COL_NONIK),
        COL_NOKK => $this->input->post(COL_NOKK),
        COL_NMNAMA => $this->input->post(COL_NMNAMA),
        COL_NMALAMAT => $this->input->post(COL_NMALAMAT),
        COL_KDKECAMATAN => $this->input->post(COL_KDKECAMATAN),
        COL_KDDESA => $this->input->post(COL_KDDESA),
        COL_NMTEMPATLAHIR => $this->input->post(COL_NMTEMPATLAHIR),
        COL_TGLLAHIR => $this->input->post(COL_TGLLAHIR),
        COL_NMJENISKELAMIN => $this->input->post(COL_NMJENISKELAMIN),
        COL_NMAYAH => $this->input->post(COL_NMAYAH),
        COL_NMIBU => $this->input->post(COL_NMIBU),
        COL_NMPENDIDIKAN => $this->input->post(COL_NMPENDIDIKAN),
        COL_NMREKANTINGGAL => $this->input->post(COL_NMREKANTINGGAL)
      );
      $det = array();
      $formKolom = $this->input->post('FormKolom');
      $formVal = $this->input->post('FormValue');

      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
      $config['max_size']	= 204800;
      $config['max_width']  = 521000;
      $config['max_height']  = 521000;
      $config['overwrite'] = FALSE;
      if(!empty($_FILES[COL_NMFOTO]["name"])) {
        $ext = explode(".", $_FILES[COL_NMFOTO]["name"]);
        $config['file_name'] = $data[COL_NONIK].".".end($ext);

        $this->load->library('upload',$config);
        if(!$this->upload->do_upload(COL_NMFOTO)){
            $err = $this->upload->display_errors();
            ShowJsonError(strip_tags($err));
            return;
        }

        $dataupload = $this->upload->data();
        if(!empty($dataupload) && $dataupload['file_name']) {
          $data[COL_NMFOTO] = $dataupload['file_name'];
        }
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TDATA, $data);
        if (!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $lastid = $this->db->insert_id();
        if(!empty($formKolom)) {
          foreach (array_keys($formKolom) as $key) {
            if(!empty($formVal[$key])){
              $det[] = array(
                COL_KDDATA=>$lastid,
                COL_KDKOLOM=>$key,
                COL_NMVALUE=>$formVal[$key]
              );
            }
          }

          if(!empty($det)) {
            $res = $this->db->insert_batch(TBL_TDATA_DETAIL, $det);
            if (!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          }
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return false;
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Success', array('redirect'=>site_url('admin/pmks/index')));
      return false;
    } else {
      $this->template->load('main', 'admin/pmks/form', $data);
    }
  }

  public function edit($id) {
    $user = GetLoggedUser();
    if($user[COL_ROLEID] != ROLEADMIN && $user[COL_ROLEID] != ROLEOPERATOR) {
      redirect('admin/dashboard/login');
    }
    $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_TDATA)
    ->row_array();
    if(empty($rdata)) {
      show_error('Data tidak ditemukan.');
      return;
    }

    $data['title'] = 'Data PMKS';
    $data['data'] = $rdata;

    if(!empty($_POST)) {
      $data = array(
        COL_KDKATEGORI => $this->input->post(COL_KDKATEGORI),
        COL_NONIK => $this->input->post(COL_NONIK),
        COL_NOKK => $this->input->post(COL_NOKK),
        COL_NMNAMA => $this->input->post(COL_NMNAMA),
        COL_NMALAMAT => $this->input->post(COL_NMALAMAT),
        COL_KDKECAMATAN => $this->input->post(COL_KDKECAMATAN),
        COL_KDDESA => $this->input->post(COL_KDDESA),
        COL_NMTEMPATLAHIR => $this->input->post(COL_NMTEMPATLAHIR),
        COL_TGLLAHIR => $this->input->post(COL_TGLLAHIR),
        COL_NMJENISKELAMIN => $this->input->post(COL_NMJENISKELAMIN),
        COL_NMAYAH => $this->input->post(COL_NMAYAH),
        COL_NMIBU => $this->input->post(COL_NMIBU),
        COL_NMPENDIDIKAN => $this->input->post(COL_NMPENDIDIKAN),
        COL_NMREKANTINGGAL => $this->input->post(COL_NMREKANTINGGAL)
      );
      $det = array();
      $formKolom = $this->input->post('FormKolom');
      $formVal = $this->input->post('FormValue');

      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
      $config['max_size']	= 204800;
      $config['max_width']  = 521000;
      $config['max_height']  = 521000;
      $config['overwrite'] = FALSE;
      if(!empty($_FILES[COL_NMFOTO]["name"])) {
        $ext = explode(".", $_FILES[COL_NMFOTO]["name"]);
        $config['file_name'] = $data[COL_NONIK].".".end($ext);

        $this->load->library('upload',$config);
        if(!$this->upload->do_upload(COL_NMFOTO)){
            $err = $this->upload->display_errors();
            ShowJsonError(strip_tags($err));
            return;
        }

        $dataupload = $this->upload->data();
        if(!empty($dataupload) && $dataupload['file_name']) {
          $data[COL_NMFOTO] = $dataupload['file_name'];
        }
      }

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TDATA, $data);
        if (!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $res = $this->db->where(COL_KDDATA, $id)->delete(TBL_TDATA_DETAIL);
        if (!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        if(!empty($formKolom)) {
          foreach (array_keys($formKolom) as $key) {
            if(!empty($formVal[$key])){
              $det[] = array(
                COL_KDDATA=>$id,
                COL_KDKOLOM=>$key,
                COL_NMVALUE=>$formVal[$key]
              );
            }
          }

          if(!empty($det)) {
            $res = $this->db->insert_batch(TBL_TDATA_DETAIL, $det);
            if (!$res) {
              $err = $this->db->error();
              throw new Exception($err['message']);
            }
          }
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return false;
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Success', array('redirect'=>site_url('admin/pmks/index')));
      return false;
    } else {
      $this->template->load('main', 'admin/pmks/form', $data);
    }
  }

  public function delete()
  {
    $user = GetLoggedUser();
    if ($user[COL_ROLEID] != ROLEADMIN && $user[COL_ROLEID] != ROLEOPERATOR) {
      redirect('site/home');
    }
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      $this->db->delete(TBL_TDATA, array(COL_UNIQ => $datum));
      $deleted++;
    }
    if ($deleted) {
      ShowJsonSuccess($deleted." data dihapus");
    } else {
      ShowJsonError("Tidak ada dihapus");
    }
  }
}
?>
