<?php
class Business extends MY_Controller {
  public $usr;

  function __construct() {
    parent::__construct();
    if(IsLogin()) {
      $this->usr = GetLoggedUser();
    }
  }

  public function load($from=null, $to=null, $limit=null) {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];

    $ruser = GetLoggedUser();
    $orderdef = array(COL_NAMA=>'asc');
    $orderables = array(TBL_M_RESTO.'.'.COL_NAMA,TBL__USERS.'.'.COL_NAMA,COL_LAT,COL_LONG);
    $cols = array(TBL_M_RESTO.'.'.COL_NAMA,TBL__USERS.'.'.COL_NAMA,COL_LAT,COL_LONG);
    $numrows = $this->db->get(TBL_M_RESTO)->num_rows();

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }
    $q = $this->db
    ->select(TBL_M_RESTO.'.*,'.TBL__USERS.'.'.COL_NAMA.' as NamaPemilik')
    ->join(TBL__USERS,TBL__USERS.'.'.COL_USERID." = ".TBL_M_RESTO.".".COL_IDPEMILIK,"left")
    ->get_compiled_select(TBL_M_RESTO, FALSE);

    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start")->result_array();
    $data = [];

    foreach($rec as $r) {
      $data[] = array(
        anchor('admin/business/edit/'.$r[COL_IDRESTO],$r[COL_NAMA]),
        $r["NamaPemilik"],
        '<i class="fa fa-'.($r[COL_ISHALAL]?'check text-success':'times text-danger').'"></i>&nbsp;HALAL&nbsp;&nbsp;<i class="fa fa-'.($r[COL_ISBUKA]?'check text-success':'times text-danger').'"></i>&nbsp;BUKA&nbsp;&nbsp;<i class="fa fa-'.($r[COL_ISAKTIF]?'check text-success':'times text-danger').'"></i>&nbsp;AKTIF',
        '<a href="#" class="btn-koordinat" data-lat="'.$r[COL_LAT].'" data-long="'.$r[COL_LONG].'" data-name="'.$r[COL_NAMA].'">'.($r[COL_LAT].', '.$r[COL_LONG]).'</a>'
      );
    }


    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $numrows,
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function index($role=0) {
    if(!IsLogin()) {
      redirect('admin/dashboard/login');
    }

    $data['role'] = $role;
    $data['title'] = 'Bisnis / Restoran';
    $this->template->load('main', 'admin/business/index', $data);
  }

  public function edit($id) {
    $rdata = $this->db
    ->where(TBL_M_RESTO.".".COL_IDRESTO, $id)
    ->get(TBL_M_RESTO)->row_array();

    if(empty($rdata)) {
      show_error('Data tidak ditemukan.');
      return;
    }

    $data['title'] = "Bisnis / Restoran";
    $data['edit'] = true;
    $data['data'] = $rdata;

    if(!empty($_POST)) {
      $userinfo = array(
        COL_NAMA => $this->input->post(COL_NAMA),
        COL_NIK => $this->input->post(COL_NIK),
        COL_NOTELP => $this->input->post(COL_NOTELP),
        COL_ALAMAT => $this->input->post(COL_ALAMAT),
        COL_TANGGALLAHIR => date('Y-m-d', strtotime($this->input->post(COL_TANGGALLAHIR))),
        COL_TANGGALREGISTRASI => date('Y-m-d')
      );

      if(!empty($_POST[COL_PASSWORD]) && !empty($_POST['ConfirmPassword'])) {
        if($_POST[COL_PASSWORD] == $_POST['ConfirmPassword']) {
          $userinfo[COL_PASSWORD] = md5($this->input->post(COL_PASSWORD));
        }
      }

      $res = true;
      $this->db->trans_begin();
      try {
        $business = array(
          COL_NAMA => $this->input->post("BusinessName"),
          COL_ALAMAT => $this->input->post("BusinessAddress"),
          COL_LAT => $this->input->post(COL_LAT),
          COL_LONG => $this->input->post(COL_LONG),
          COL_ISHALAL => $this->input->post(COL_ISHALAL) == 'on',
          COL_ISBUKA => $this->input->post(COL_ISBUKA) == 'on',
          COL_ISAKTIF => $this->input->post(COL_ISAKTIF) == 'on'
        );
        $res = $this->db->where(COL_IDRESTO, $id)->update(TBL_M_RESTO, $business);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $arrSchedule = array();
        $postSchedule = $this->input->post("JadwalBuka");
        if(!empty($postSchedule)) {
          $postSchedule = json_decode(urldecode($postSchedule));
          foreach($postSchedule as $s) {
            $arrSchedule[] = array(
              COL_IDRESTO=>$id,
              COL_IDHARI=>$s->Day,
              COL_JAMBUKA=>$s->TimeFrom,
              COL_JAMTUTUP=>$s->TimeTo
            );
          }

          $res = $this->db->where(COL_IDRESTO, $id)->delete(TBL_M_RESTOJADWAL);
          $res = $this->db->insert_batch(TBL_M_RESTOJADWAL, $arrSchedule);
          if(!$res) {
            throw new Exception('Error: '.$this->db->error());
          }
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Berhasil', array('redirect'=>site_url('admin/business/index')));
        return;
      } catch (Exception $e) {
        $this->db->trans_rollback();
        ShowJsonError($e->getMessage());
        return;
      }
    }
    $this->template->load('main', 'admin/business/form', $data);
  }

  public function menu($id) {
    if(!IsLogin()) {
      redirect('admin/dashboard/login');
    }

    $rdata = $this->db
    ->where(TBL_M_RESTO.".".COL_IDRESTO, $id)
    ->get(TBL_M_RESTO)->row_array();

    if(empty($rdata)) {
      show_error('Data tidak ditemukan.');
      return;
    }

    $data['data'] = $rdata;
    $data['menu'] = $this->db->where(COL_IDRESTO, $id)->order_by(COL_IDMENU)->get(TBL_M_RESTOMENU)->result_array();
    $this->load->view('admin/business/_index_menu', $data);
  }

  public function add_menu($id) {
    if(!IsLogin()) {
      redirect('admin/dashboard/login');
    }

    $rdata = $this->db
    ->where(TBL_M_RESTO.".".COL_IDRESTO, $id)
    ->get(TBL_M_RESTO)->row_array();

    if(empty($rdata)) {
      show_error('Data tidak ditemukan.');
      return;
    }

    if(!empty($_POST)) {
      $menu = array(
        COL_IDRESTO => $id,
        COL_MENU=>$this->input->post(COL_MENU),
        COL_DESKRIPSI=>$this->input->post(COL_DESKRIPSI),
        COL_HARGA=>!empty($this->input->post(COL_HARGA)) ? toNum($this->input->post(COL_HARGA)) : 0,
        COL_ISTERSEDIA=>$this->input->post(COL_ISTERSEDIA) == 'on',
        COL_ISAKTIF=>$this->input->post(COL_ISAKTIF) == 'on',
        COL_CREATEDON => date('Y-m-d H:i:s'),
        COL_CREATEDBY => $this->usr[COL_EMAIL]
      );

      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
      $config['max_size']	= 5000;
      $config['max_width']  = 2048;
      $config['max_height']  = 2048;
      $config['overwrite'] = TRUE;
      if(!empty($_FILES[COL_FILEGAMBAR]["name"])) {
        $fname = 'menu-'.str_pad($id, 5, '0', STR_PAD_LEFT).'-'.date('YmdHis');
        if(!empty($fname)) {
          $config['file_name'] = strtolower($fname).".".end((explode(".", $_FILES[COL_FILEGAMBAR]["name"])));
        }

        $this->load->library('upload',$config);
        if(!$this->upload->do_upload(COL_FILEGAMBAR)){
            $err = $this->upload->display_errors();
            ShowJsonError(strip_tags($err));
            return;
        }

        $dataupload = $this->upload->data();
        if(!empty($dataupload) && $dataupload['file_name']) {
          $menu[COL_FILEGAMBAR] = $dataupload['file_name'];
          /*if(!empty($rmenu[COL_FILEGAMBAR]) && file_exists(MY_UPLOADPATH.$rmenu[COL_FILEGAMBAR])) {
            unlink(MY_UPLOADPATH.$rmenu[COL_FILEGAMBAR]);
          }*/
        }
      }

      $res = $this->db->insert(TBL_M_RESTOMENU, $menu);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError('Error: '.$err['message']);
        return;
      }

      ShowJsonSuccess('Berhasil', array('redirect'=>site_url('admin/business/edit/'.$id)));
      return;
    }

    $data['data'] = $rdata;
    $this->load->view('admin/business/_form_menu', $data);
  }

  public function edit_menu($id) {
    if(!IsLogin()) {
      redirect('admin/dashboard/login');
    }

    $rmenu = $this->db
    ->where(TBL_M_RESTOMENU.".".COL_IDMENU, $id)
    ->get(TBL_M_RESTOMENU)->row_array();

    if(empty($rmenu)) {
      show_error('Data tidak ditemukan.');
      return;
    }

    $rdata = $this->db
    ->where(TBL_M_RESTO.".".COL_IDRESTO, $rmenu[COL_IDRESTO])
    ->get(TBL_M_RESTO)->row_array();

    if(empty($rdata)) {
      show_error('Data tidak ditemukan.');
      return;
    }

    if(!empty($_POST)) {
      $menu = array(
        COL_MENU=>$this->input->post(COL_MENU),
        COL_DESKRIPSI=>$this->input->post(COL_DESKRIPSI),
        COL_HARGA=>!empty($this->input->post(COL_HARGA)) ? toNum($this->input->post(COL_HARGA)) : 0,
        COL_ISTERSEDIA=>$this->input->post(COL_ISTERSEDIA) == 'on',
        COL_ISAKTIF=>$this->input->post(COL_ISAKTIF) == 'on',
        COL_CREATEDON => date('Y-m-d H:i:s'),
        COL_CREATEDBY => $this->usr[COL_EMAIL]
      );

      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = UPLOAD_ALLOWEDTYPES;
      $config['max_size']	= 5000;
      $config['max_width']  = 2048;
      $config['max_height']  = 2048;
      $config['overwrite'] = TRUE;
      if(!empty($_FILES[COL_FILEGAMBAR]["name"])) {
        $fname = 'menu-'.str_pad($id, 5, '0', STR_PAD_LEFT).'-'.date('YmdHis');
        if(!empty($fname)) {
          $config['file_name'] = strtolower($fname).".".end((explode(".", $_FILES[COL_FILEGAMBAR]["name"])));
        }

        $this->load->library('upload',$config);
        if(!$this->upload->do_upload(COL_FILEGAMBAR)){
            $err = $this->upload->display_errors();
            ShowJsonError(strip_tags($err));
            return;
        }

        $dataupload = $this->upload->data();
        if(!empty($dataupload) && $dataupload['file_name']) {
          $menu[COL_FILEGAMBAR] = $dataupload['file_name'];
          if(!empty($rmenu[COL_FILEGAMBAR]) && file_exists(MY_UPLOADPATH.$rmenu[COL_FILEGAMBAR])) {
            unlink(MY_UPLOADPATH.$rmenu[COL_FILEGAMBAR]);
          }
        }
      }

      $res = $this->db->where(COL_IDMENU, $id)->update(TBL_M_RESTOMENU, $menu);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError('Error: '.$err['message']);
        return;
      }

      ShowJsonSuccess('Berhasil', array('redirect'=>site_url('admin/business/edit/'.$rmenu[COL_IDRESTO])));
      return;
    }

    $data['data'] = $rdata;
    $data['menu'] = $rmenu;
    $this->load->view('admin/business/_form_menu', $data);
  }

  public function delete_menu($id) {
    $rmenu = $this->db
    ->where(COL_IDMENU, $id)
    ->get(TBL_M_RESTOMENU)
    ->row_array();

    $res = $this->db->where(COL_IDMENU, $id)->delete(TBL_M_RESTOMENU);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      return;
    }

    if(!empty($rmenu) && file_exists(MY_UPLOADPATH.$rmenu[COL_FILEGAMBAR])) {
      unlink(MY_UPLOADPATH.$rmenu[COL_FILEGAMBAR]);
    }
    ShowJsonSuccess('OK');
    return;
  }
}
?>
