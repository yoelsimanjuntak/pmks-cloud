<?php
class Report extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin() ) {
      redirect('site/home');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEOPERATOR) {
      redirect('admin/dashboard/login');
    }
  }

  public function rekap_kabupaten() {
    $rkategori = $this->db->order_by(COL_NMKATEGORI)->get(TBL_MKATEGORI)->result_array();
    $qkategori = "'' as dumpcol";
    if(!empty($rkategori)) {
      $qkategori = "";
      for($i=0; $i<count($rkategori); $i++) {
        $qkategori .= "sum(IF(dat.KdKategori=".$rkategori[$i][COL_KDKATEGORI].", 1, 0)) as Total_".$rkategori[$i][COL_KDKATEGORI];
        if($i != count($rkategori) -1) {
          $qkategori .= ", ";
        }
      }
    }

    $q = @"
select
kec.NmKecamatan,
count(*) as Total,
$qkategori
from tdata dat
left join mkecamatan kec on kec.KdKecamatan = dat.KdKecamatan
group by
kec.NmKecamatan
order by
kec.NmKecamatan
    ";

    $data['cetak'] = $cetak = $this->input->get("cetak");
    $data['title'] = 'Rekapitulasi Kabupaten';
    $data['res'] = $this->db->query($q)->result_array();
    $data['rkategori'] = $rkategori;

    if($cetak) $this->load->view('admin/report/rekap-kabupaten_', $data);
    else $this->template->load('main', 'admin/report/rekap-kabupaten', $data);
  }

  public function rekap_kecamatan() {
    $rkategori = $this->db->order_by(COL_NMKATEGORI)->get(TBL_MKATEGORI)->result_array();
    if(!empty($_GET) && !empty($_GET[COL_KDKECAMATAN])) {
      $qkecamatan = $this->input->get(COL_KDKECAMATAN);
      $qkategori = "'' as dumpcol";
      if(!empty($rkategori)) {
        $qkategori = "";
        for($i=0; $i<count($rkategori); $i++) {
          $qkategori .= "sum(IF(dat.KdKategori=".$rkategori[$i][COL_KDKATEGORI].", 1, 0)) as Total_".$rkategori[$i][COL_KDKATEGORI];
          if($i != count($rkategori) -1) {
            $qkategori .= ", ";
          }
        }
      }

      $q = @"
      select
      des.NmDesa,
      count(*) as Total,
      $qkategori
      from tdata dat
      left join mdesa des on des.KdDesa = dat.KdDesa
      where
        dat.KdKecamatan = $qkecamatan
      group by
        des.NmDesa
      order by
        des.NmDesa
      ";

      $data['res'] = $this->db->query($q)->result_array();
    }

    $data['cetak'] = $cetak = $this->input->get("cetak");
    $data['title'] = 'Rekapitulasi Kabupaten';
    $data['rkategori'] = $rkategori;

    if($cetak) $this->load->view('admin/report/rekap-kecamatan_', $data);
    else $this->template->load('main', 'admin/report/rekap-kecamatan', $data);
  }

  public function rekap_kategori() {
    $idKategori = $this->input->get(COL_KDKATEGORI);
    $idKecamatan = $this->input->get(COL_KDKECAMATAN);
    $idDesa = $this->input->get(COL_KDDESA);
    $arrKolom = $this->input->get('Kolom');
    $arrKolomDet = $this->input->get('KdKolom');

    if(!empty($idKategori) && !empty($idKategori) && !empty($idDesa)) {
      $data['rkategori'] = $rkategori = $this->db->where(COL_KDKATEGORI, $idKategori)->get(TBL_MKATEGORI)->row_array();
      $data['rkecamatan'] = $rkecamatan = $this->db->where(COL_KDKECAMATAN, $idKecamatan)->get(TBL_MKECAMATAN)->row_array();
      $data['rdesa'] = $rdesa = $this->db->where(COL_KDDESA, $idDesa)->get(TBL_MDESA)->row_array();
      $data['kol'] = $arrKolom;
      $data['kdKolom'] = $arrKolomDet;

      if(!empty($rkategori) && !empty($rkecamatan) && !empty($rdesa) && !empty($arrKolom) && !empty($arrKolomDet)) {
        $qjoin = "";
        $qselect = "";
        foreach($arrKolomDet as $kol) {
          $qselect .= @", t_$kol.NmValue as kol_$kol";
          $qjoin .= @" left join tdata_detail t_$kol on t_$kol.KdData = tdata.Uniq and t_$kol.KdKolom = $kol ";
        }
        $q = @"select
        tdata.* $qselect
        from tdata $qjoin
        where
          tdata.KdKategori = $idKategori
          and tdata.KdKecamatan = $idKecamatan
          and tdata.KdDesa = $idDesa
        ";
        $data['res'] = $this->db->query($q)->result_array();
        //echo $q;
        //return;
      }
    }

    $data['cetak'] = $cetak = $this->input->get("cetak");
    $data['title'] = 'Rekapitulasi Kategori';

    if($cetak) $this->load->view('admin/report/rekap-kategori_', $data);
    else $this->template->load('main', 'admin/report/rekap-kategori', $data);
  }

  public function rekap_kategori_v2() {
    $idKategori = $this->input->get(COL_KDKATEGORI);
    $idKecamatan = $this->input->get(COL_KDKECAMATAN);
    $arrKolomDet_ = array();

    if(!empty($idKategori) && !empty($idKecamatan)) {
      $data['rkategori'] = $rkategori = $this->db->where(COL_KDKATEGORI, $idKategori)->get(TBL_MKATEGORI)->row_array();
      $data['rkecamatan'] = $rkecamatan = $this->db->where(COL_KDKECAMATAN, $idKecamatan)->get(TBL_MKECAMATAN)->row_array();

      if(!empty($rkategori) && !empty($rkecamatan)) {
        $arrKolomDet = $this->db
        ->where(COL_KDKATEGORI, $idKategori)
        ->order_by(COL_KDPARENT)
        ->order_by(COL_SEQ)
        ->get(TBL_MKOLOM)
        ->result_array();

        $qjoin = "";
        $qselect = "";
        foreach($arrKolomDet as $kol_) {
          $kol = $kol_[COL_KDKOLOM];
          $arrKolomDet_[] = $kol;

          $qselect .= @", t_$kol.NmValue as kol_$kol";
          $qjoin .= @" left join tdata_detail t_$kol on t_$kol.KdData = tdata.Uniq and t_$kol.KdKolom = $kol ";
        }
        $q = @"select
        tdata.*, des.NmDesa $qselect
        from tdata $qjoin
        left join mdesa des on des.KdDesa = tdata.KdDesa
        where
          tdata.KdKategori = $idKategori
          and tdata.KdKecamatan = $idKecamatan
        ";
        $data['res'] = $this->db->query($q)->result_array();
        $data['kdKolom'] = $arrKolomDet_;
        //echo $q;
        //return;
      }
    }

    $data['cetak'] = $cetak = $this->input->get("cetak");
    $data['title'] = 'Rekapitulasi Kategori';

    if($cetak) $this->load->view('admin/report/rekap-kategori-v2_', $data);
    else $this->template->load('main', 'admin/report/rekap-kategori-v2', $data);
  }
}
?>
