<?php
class Master extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin() ) {
      redirect('site/home');
    }

    $ruser = GetLoggedUser();
    if($ruser[COL_ROLEID] != ROLEADMIN && $ruser[COL_ROLEID] != ROLEOPERATOR) {
      redirect('admin/dashboard/login');
    }
  }

  public function index_kecamatan() {
    $data['title'] = 'Kecamatan';
    $data['res'] = $this->db->get(TBL_MKECAMATAN)->result_array();
    $this->template->load('main', 'admin/master/index_kecamatan', $data);
  }

  public function index_desa() {
    $data['title'] = 'Desa';
    $data['res'] = $this->db->join(TBL_MKECAMATAN,TBL_MKECAMATAN.'.'.COL_KDKECAMATAN." = ".TBL_MDESA.".".COL_KDKECAMATAN,"left")->get(TBL_MDESA)->result_array();
    $this->template->load('main', 'admin/master/index_desa', $data);
  }

  public function index_kategori() {
    $data['title'] = 'Kategori';
    $data['res'] = $this->db->get(TBL_MKATEGORI)->result_array();
    $this->template->load('main', 'admin/master/index_kategori', $data);
  }

  public function index_kolom() {
    $data['title'] = 'Kolom';
    $data['res'] = $this->db
    ->select('mkolom.*, mkategori.NmKategori, mkategori.Seq as SeqKategori')
    ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_KDKATEGORI." = ".TBL_MKOLOM.".".COL_KDKATEGORI,"left")
    ->order_by(TBL_MKATEGORI.'.'.COL_SEQ, 'asc')
    ->order_by(TBL_MKOLOM.'.'.COL_SEQ, 'asc')
    ->get(TBL_MKOLOM)
    ->result_array();
    $this->template->load('main', 'admin/master/index_kolom', $data);
  }

  public function add_kecamatan() {
    $user = GetLoggedUser();
    if($user[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashboard/login');
    }
    if(!empty($_POST)) {
      $data = array(
        COL_NMKECAMATAN => $this->input->post(COL_NMKECAMATAN)
      );

      $res = $this->db->insert(TBL_MKECAMATAN, $data);
      if ($res) {
        ShowJsonSuccess("Berhasil");
      } else {
        ShowJsonError("Gagal");
      }
    }
  }

  public function add_desa() {
    $user = GetLoggedUser();
    if($user[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashboard/login');
    }
    if(!empty($_POST)) {
      $data = array(
        COL_KDKECAMATAN => $this->input->post(COL_KDKECAMATAN),
        COL_NMDESA => $this->input->post(COL_NMDESA)
      );

      $res = $this->db->insert(TBL_MDESA, $data);
      if ($res) {
        ShowJsonSuccess("Berhasil");
      } else {
        ShowJsonError("Gagal");
      }
    }
  }

  public function add_kategori() {
    $user = GetLoggedUser();
    if($user[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashboard/login');
    }
    if(!empty($_POST)) {
      $data = array(
        COL_NMKATEGORI => $this->input->post(COL_NMKATEGORI),
        COL_SEQ => $this->input->post(COL_SEQ)
      );

      $res = $this->db->insert(TBL_MKATEGORI, $data);
      if ($res) {
        ShowJsonSuccess("Berhasil");
      } else {
        ShowJsonError("Gagal");
      }
    }
  }

  public function edit_kecamatan($id) {
    $user = GetLoggedUser();
    if ($user[COL_ROLEID] != ROLEADMIN) {
      redirect('site/home');
    }
    if (!empty($_POST)) {
      $data = array(
        COL_NMKECAMATAN => $this->input->post(COL_NMKECAMATAN)
      );

      $res = $this->db->where(COL_KDKECAMATAN, $id)->update(TBL_MKECAMATAN, $data);
      if ($res) {
        ShowJsonSuccess("Berhasil");
      } else {
        ShowJsonError("Gagal");
      }
    }
  }

  public function edit_desa($id) {
    $user = GetLoggedUser();
    if ($user[COL_ROLEID] != ROLEADMIN) {
      redirect('site/home');
    }
    if (!empty($_POST)) {
      $data = array(
        COL_KDKECAMATAN => $this->input->post(COL_KDKECAMATAN),
        COL_NMDESA => $this->input->post(COL_NMDESA)
      );

      $res = $this->db->where(COL_KDDESA, $id)->update(TBL_MDESA, $data);
      if ($res) {
        ShowJsonSuccess("Berhasil");
      } else {
        ShowJsonError("Gagal");
      }
    }
  }

  public function edit_kategori($id) {
    $user = GetLoggedUser();
    if ($user[COL_ROLEID] != ROLEADMIN) {
      redirect('site/home');
    }
    if (!empty($_POST)) {
      $data = array(
        COL_NMKATEGORI => $this->input->post(COL_NMKATEGORI),
        COL_SEQ => $this->input->post(COL_SEQ)
      );

      $res = $this->db->where(COL_KDKATEGORI, $id)->update(TBL_MKATEGORI, $data);
      if ($res) {
        ShowJsonSuccess("Berhasil");
      } else {
        ShowJsonError("Gagal");
      }
    }
  }

  public function delete_kecamatan()
  {
    $user = GetLoggedUser();
    if ($user[COL_ROLEID] != ROLEADMIN) {
      redirect('site/home');
    }
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      $this->db->delete(TBL_MKECAMATAN, array(COL_KDKECAMATAN => $datum));
      $deleted++;
    }
    if ($deleted) {
      ShowJsonSuccess($deleted." data dihapus");
    } else {
      ShowJsonError("Tidak ada dihapus");
    }
  }

  public function delete_desa()
  {
    $user = GetLoggedUser();
    if ($user[COL_ROLEID] != ROLEADMIN) {
      redirect('site/home');
    }
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      $this->db->delete(TBL_MDESA, array(COL_KDDESA => $datum));
      $deleted++;
    }
    if ($deleted) {
      ShowJsonSuccess($deleted." data dihapus");
    } else {
      ShowJsonError("Tidak ada dihapus");
    }
  }

  public function delete_kategori()
  {
    $user = GetLoggedUser();
    if ($user[COL_ROLEID] != ROLEADMIN) {
      redirect('site/home');
    }
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      $this->db->delete(TBL_MKATEGORI, array(COL_KDKATEGORI => $datum));
      $deleted++;
    }
    if ($deleted) {
      ShowJsonSuccess($deleted." data dihapus");
    } else {
      ShowJsonError("Tidak ada dihapus");
    }
  }

  public function add_kolom() {
    $user = GetLoggedUser();
    if($user[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashboard/login');
    }

    $data['title'] = 'Kolom';
    $data['def'][COL_NMOPTION] = json_encode(array());
    $data['def']['ArrChild'] = json_encode(array());
    if(!empty($_POST)) {
      $arrChild = urldecode($this->input->post('ArrChild'));
      $data = array(
        COL_KDKATEGORI => $this->input->post(COL_KDKATEGORI),
        COL_NMKOLOM => $this->input->post(COL_NMKOLOM),
        COL_SEQ => $this->input->post(COL_SEQ),
        COL_ISALLOWEMPTY => $this->input->post(COL_ISALLOWEMPTY) == 'on',
        COL_NMTIPE => $this->input->post(COL_NMTIPE),
        COL_NMOPTION => !empty($this->input->post(COL_NMOPTION)) ? urldecode($this->input->post(COL_NMOPTION)) : null
      );

      $res = $this->db->insert(TBL_MKOLOM, $data);
      if (!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        return false;
      }

      if(!empty($arrChild)) {
        $arrChild_ = array();
        $arrChild = json_decode($arrChild);
        $lastid = $this->db->insert_id();

        if(count($arrChild) > 0) {
          foreach($arrChild as $c) {
            $arrChild_[] = array(
              COL_KDPARENT=>$lastid,
              COL_KDKATEGORI=>$data[COL_KDKATEGORI],
              COL_KDCONDITION=>$c->ChildCond,
              COL_SEQ=>$c->ChildNo,
              COL_NMKOLOM=>$c->ChildName,
              COL_NMTIPE=>"SELECT",
              COL_NMOPTION=>json_encode(explode(",", $c->ChildOpt))
            );
          }
        }
        if(count($arrChild_) > 0) {
          $res = $this->db->insert_batch(TBL_MKOLOM, $arrChild_);
          if (!$res) {
            $err = $this->db->error();
            ShowJsonError($err['message']);
            return false;
          }
        }
      }
      ShowJsonSuccess('Success', array('redirect'=>site_url('admin/master/index-kolom')));
      return false;
    } else {
      $this->template->load('main', 'admin/master/form_kolom', $data);
    }
  }

  public function edit_kolom($id) {
    $user = GetLoggedUser();
    if($user[COL_ROLEID] != ROLEADMIN) {
      redirect('admin/dashboard/login');
    }
    $rdata = $this->db
    ->where(COL_KDKOLOM, $id)
    ->get(TBL_MKOLOM)
    ->row_array();
    if(empty($rdata)) {
      show_error('Data tidak ditemukan.');
      return;
    }

    $arrChild_ = array();
    $arrchild = $this->db->where(COL_KDPARENT, $id)->get(TBL_MKOLOM)->result_array();
    if(!empty($arrchild)) {
      foreach ($arrchild as $c) {
        $arrChild_[] = array(
          'ChildNo'=>$c[COL_SEQ],
          'ChildName'=>$c[COL_NMKOLOM],
          'ChildCond'=>$c[COL_KDCONDITION],
          'ChildOpt'=>!empty($c[COL_NMOPTION])?implode(',',json_decode($c[COL_NMOPTION])):''
        );
      }
    }
    $data['title'] = 'Kolom';
    $data['data'] = $rdata;
    $data['def'][COL_NMOPTION] = $rdata[COL_NMOPTION];
    $data['def']['ArrChild'] = json_encode($arrChild_);

    if(!empty($_POST)) {
      $arrChild = urldecode($this->input->post('ArrChild'));
      $data = array(
        COL_KDKATEGORI => $this->input->post(COL_KDKATEGORI),
        COL_KDCONDITION => $this->input->post(COL_KDCONDITION),
        COL_NMKOLOM => $this->input->post(COL_NMKOLOM),
        COL_SEQ => $this->input->post(COL_SEQ),
        COL_ISALLOWEMPTY => $this->input->post(COL_ISALLOWEMPTY) == 'on',
        COL_NMTIPE => $this->input->post(COL_NMTIPE),
        COL_NMOPTION => !empty($this->input->post(COL_NMOPTION)) ? urldecode($this->input->post(COL_NMOPTION)) : null
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_KDKOLOM, $id)->update(TBL_MKOLOM, $data);
        if (!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $res = $this->db->where(COL_KDPARENT, $id)->delete(TBL_MKOLOM);
        if (!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        if(!empty($arrChild)) {
          $arrChild_ = array();
          $arrChild = json_decode($arrChild);

          if(count($arrChild) > 0) {
            foreach($arrChild as $c) {
              $arrChild_[] = array(
                COL_KDPARENT=>$id,
                COL_KDKATEGORI=>$data[COL_KDKATEGORI],
                COL_KDCONDITION=>$c->ChildCond,
                COL_SEQ=>$c->ChildNo,
                COL_NMKOLOM=>$c->ChildName,
                COL_NMTIPE=>"SELECT",
                COL_NMOPTION=>json_encode(explode(",", $c->ChildOpt))
              );
            }
          }
          if(count($arrChild_) > 0) {
            $res = $this->db->insert_batch(TBL_MKOLOM, $arrChild_);
            if (!$res) {
              $$err = $this->db->error();
              throw new Exception($err['message']);
            }
          }
        }
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return false;
      }

      $this->db->trans_commit();
      ShowJsonSuccess('Success', array('redirect'=>site_url('admin/master/index-kolom')));
      return false;
    } else {
      $this->template->load('main', 'admin/master/form_kolom', $data);
    }
  }

  public function delete_kolom()
  {
    $user = GetLoggedUser();
    if ($user[COL_ROLEID] != ROLEADMIN) {
      redirect('site/home');
    }
    $data = $this->input->post('cekbox');
    $deleted = 0;
    foreach ($data as $datum) {
      $this->db->delete(TBL_MKOLOM, array(COL_KDKOLOM => $datum));
      $deleted++;
    }
    if ($deleted) {
      ShowJsonSuccess($deleted." data dihapus");
    } else {
      ShowJsonError("Tidak ada dihapus");
    }
  }

  public function get_opt_desa() {
    $kdKec = $this->input->post('KdKec');
    $sel = $this->input->post('KdDesa');
    $emptytext = $this->input->post('emptyText');

    $cond = !empty($kdKec)?"where KdKecamatan = $kdKec":"";
    echo GetCombobox("SELECT * from mdesa $cond order by NmDesa", COL_KDDESA, COL_NMDESA, (!empty($sel)?$sel:null), (!empty($emptytext)?true:false), false, (!empty($emptytext)?$emptytext:''));
  }

  public function get_kategori_form() {
    $id = $this->input->post('KdKategori');
    $rkolom = $this->db
    ->where(COL_KDKATEGORI, $id)
    ->where(COL_KDPARENT, null)
    ->order_by(COL_SEQ)
    ->get(TBL_MKOLOM)
    ->result_array();
    if(!empty($rkolom)) {
      $this->load->view('admin/master/form_', array('rkolom'=>$rkolom));
    }
  }

  public function get_kategori_opt() {
    $id = $this->input->post('KdKategori');

    $cond = !empty($id)?"where KdKategori = $id":"";
    echo GetCombobox("SELECT * from mkolom $cond order by KdParent, Seq", COL_KDKOLOM, COL_NMKOLOM, null);
  }

  public function get_kategori_child() {
    $id = $this->input->post('KdKolom');
    $cond = $this->input->post('KdCond');
    $dat = $this->input->post('KdData');
    $rkolom = $this->db
    ->where(COL_KDPARENT, $id)
    ->where(COL_KDCONDITION, $cond)
    ->order_by(COL_SEQ)
    ->get(TBL_MKOLOM)
    ->result_array();
    if(!empty($rkolom)) {
      $this->load->view('admin/master/form_', array('rkolom'=>$rkolom, 'KdDat'=>$dat));
    }
  }
}
?>
